import re
from datetime import datetime, timedelta

from django.db.models import Q
from django.utils import timezone

from apps.payment.models import Ticket, Sale
from apps.seats.models import ZoneSeat, Section, Seat
from apps.web.models import Zone

piso1_grid = {
    'P1': 73,
}
piso2_grid = {
    'P2': 74 - 119
}
piso_zone = Zone.objects.get(id=643)
piso_section = Section.objects.get(id=14)

def set_piso1():
    letter = 'P1'
    for number in range(1, 74):
        seat = Seat.objects.create(section=piso_section, letter=letter, number=number)
        ZoneSeat.objects.create(zone=piso_zone, seat=seat)

def set_piso2():
    letter = 'P2'
    for number in range(74, 132):
        seat = Seat.objects.create(section=piso_section, letter=letter, number=number)
        ZoneSeat.objects.create(zone=piso_zone, seat=seat)