#!/usr/bin/env bash

DJANGO_DIR=/home/devops/projects/fuzzpass

# Activate the virtual environment
source /home/devops/.virtualenvs/fuzzpass/bin/activate

# Set additional environment variables.
export DJANGO_SETTINGS_MODULE=fuzzpass.settings.production
cd $DJANGO_DIR

exec /home/devops/.virtualenvs/fuzzpass/bin/celery flower --port=5555 --persistent=True --basic_auth=fuzzpass:FL46wOx6Z3ZR2rtp --broker=redis://guest:guest@localhost:6379/0 --url_prefix=flower

