$(document).ready(function () {
    $("#more_content").click(function () {
        $("#detail_content").slideToggle();
    });

    $("#more_content2").click(function () {
        $("#detail_content2").slideToggle();
    });

    $(".profile-edit").click(function () {
        $(".user-data").toggle();
        $(".user-data-edit").toggle();
        $(this).css({
            'visibility': 'hidden'
        });
    });

    $(".data-edit").click(function () {
        $(".user-data").toggle();
        $(".user-data-edit").toggle();
        $('.profile-edit').css({
            'visibility': 'hidden'
        });
    });

    $('#user-edit-cancel').click(function () {
        $('.user-data').toggle();
        $('.user-data-edit').toggle();
        $('.profile-edit').css({
            'visibility': 'visible'
        });
    });

    $('#header__icon').click(function (e) {
        e.preventDefault();
        $('body').toggleClass('with--sidebar');
    });

    $('#site-cache').click(function (e) {
        $('body').removeClass('with--sidebar');
    });

    $('.js__settings').on('click', function (e) {
        $('.profile__settings').toggle();
        e.stopPropagation();
    });

    $('.profile__settings').on('click', function (e) {
        e.stopPropagation();
    });

    $("body").click(function (e) {
        $(".profile__settings").hide();
    });

    $('.header__mobile').on('click', function (e) {
        $(".mobile__nav").toggle();
        e.stopPropagation();
    });

    $('.mobile__nav').on('click', function (e) {
        e.stopPropagation();
    });

    $("body").click(function (e) {
        $(".mobile__nav").hide();
    });

    // $('.carousel').carousel({
    //     interval: 32000
    // });


    var formAjaxSubmit = function (form, modal) {
        $(form).submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (xhr, ajaxOptions, thrownError) {
                    if ($(xhr).find('.has-error').length > 0) {
                        $(modal).find('.modal-body').html(xhr);
                        formAjaxSubmit(form, modal);
                        $('#form-register select').removeClass("select form-control").addClass("custom-dropdown__select");
                        $('#id_first_name').attr("placeholder", 'Nombres');
                        $('#id_last_name').attr("placeholder", 'Apellidos');
                        $('#id_birthday').attr("placeholder", 'Fecha de Nacimiento (AAAA-MM-DD)');
                        $('#id_dni').attr("placeholder", 'DNI');
                        $('#form-register #id_email').attr("placeholder", 'Correo');
                        $('#form-register #id_password').attr("placeholder", 'Contraseña');
                        $('.custom-dropdown__select option:selected').text('Sexo');
                        $('#div_id_musicals .control-label').text('Estilos Musicales');
                        $('#form-register input:submit').val('Registrar');
                        $('#div_id_dni').append("<div class='help-tip'><p>Necesitamos tu documento de identidad para poder validar tus compras.</p></div>");
                        $('#id_email').attr("placeholder", $.parseHTML("&#xf1d8;")[0].data + ' Correo').addClass('empty');
                        $('#id_password').attr("placeholder", $.parseHTML("&#xf023;")[0].data + ' Contraseña').addClass('empty');
                        loadValidatorDate();
                    } else {
                        $('.onEnvThird').addClass('fa-spinner-active');
                        window.location.href = next;
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr, thrownError);
                }
            });
        });
    };

    var registerFormAjaxSubmit = function (form, modal) {
        $(form).submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: $(this).attr('method'),
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (xhr, ajaxOptions, thrownError) {
                    if ($(xhr).find('.has-error').length > 0) {
                        $(modal).find('.modal-body').html(xhr);
                        formAjaxSubmit(form, modal);
                        $('#form-register select').removeClass("select form-control").addClass("custom-dropdown__select");
                        $('#id_first_name').attr("placeholder", 'Nombres');
                        $('#id_last_name').attr("placeholder", 'Apellidos');
                        $('#id_birthday').attr("placeholder", 'Fecha de Nacimiento (AAAA-MM-DD)');
                        $('#id_dni').attr("placeholder", 'DNI');
                        $('#form-register #id_email').attr("placeholder", 'Correo');
                        $('#form-register #id_password').attr("placeholder", 'Contraseña');
                        $('.custom-dropdown__select option:selected').text('Sexo');
                        $('#div_id_musicals .control-label').text('Estilos Musicales');
                        $('#form-register input:submit').val('Registrar');
                        $('#div_id_dni').append("<div class='help-tip'><p>Necesitamos tu documento de identidad para poder validar tus compras.</p></div>");
                        $('#id_email').attr("placeholder", $.parseHTML("&#xf1d8;")[0].data + ' Correo').addClass('empty');
                        $('#id_password').attr("placeholder", $.parseHTML("&#xf023;")[0].data + ' Contraseña').addClass('empty');
                        loadValidatorDate();
                    } else {
                        $('.onEnvThird').addClass('fa-spinner-active');

                        if (typeof nextparams !== "undefined") {
                            if (nextparams.indexOf('/buy-from_influence/') >= 0) {
                                nextparams = '/f_i/' + nextparams.split('/')[2] + '/' + nextparams.split('/')[3] + '/';
                            } else {
                                nextparams = nextparams.replace("buy-in", "eventos");
                            }
                        } else {
                            nextparams = "/";
                        }
                        window.location.href = nextparams;
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr, thrownError);
                }
            });
        });
    };

    $('#form-resetpass #user_email').attr("placeholder", $.parseHTML("&#xf1d8;")[0].data + ' Correo').addClass('empty');
    $('#btn-login').click(function () {
        $('#form-login-body').load('/test-login/', function () {
            $('#form-login-body input:submit').val("Entrar");
            $('#id_email').attr("placeholder", $.parseHTML("&#xf1d8;")[0].data + ' Correo').addClass('empty');
            $('#id_password').attr("placeholder", $.parseHTML("&#xf023;")[0].data + ' Contraseña').addClass('empty');
            $('#form-login').modal('toggle');
            next = window.location.pathname;
            formAjaxSubmit('#form-login-body form', '#form-login');

            $('input:submit').click(function () {
                $('#id_email').attr("placeholder", $.parseHTML("&#xf1d8;")[0].data + ' Correo').addClass('empty');
                $('#id_password').attr("placeholder", $.parseHTML("&#xf023;")[0].data + ' Contraseña').addClass('empty');
            })
        });
    });


    /*
     * Formulario de registro - Modal
     *
     */

    var next, nextparams, urlOrigin, urlOrigin_influence, urlOrigin_event, inputInfluence, inputEvent;

    $('#btn-register').click(function () {
        $('#form-register-modal-body').load('/test-register/', function () {
            /*
             * Rellenar datos del formulario
             * si en caso la URL es de influencer
             */

            urlOrigin = location.search.split('next=')[1];
            if (typeof urlOrigin !== "undefined") {
                if (urlOrigin.indexOf('/buy-from_influence/') >= 0) {
                    urlOrigin_influence = urlOrigin.split('/')[2];
                    urlOrigin_event = urlOrigin.split('/')[3];
                }
                else {
                    urlOrigin_influence = '';
                    urlOrigin_event = ''
                }
            }

            /* ========== Otra lógica =======*/

            $('.selectmultiple').removeAttr('id');
            $('.selectmultiple').removeAttr('multiple');
            $('.selectmultiple').attr('id', 'ms');
            $('.selectmultiple').removeClass();
            $('.selectmultiple').addClass('custom-dropdown__select');
            $('#form-register select').removeClass("select form-control").addClass("custom-dropdown__select");
            $('#id_gender option:selected').text('Sexo');
            $('#form-register input:submit').addClass('full-width');
            $('#form-register input:submit').val('Registrar');
            $('.custom-footer input:submit').val('Entrar con Facebook');
            $('#form-register #id_first_name').attr("placeholder", 'Nombre');
            $('#form-register #id_last_name').attr("placeholder", 'Apellido');
            $('#form-register #id_email').attr("placeholder", 'Correo');
            $('#div_id_dni').append("<div class='help-tip'><p>Necesitamos tu documento de identidad para poder validar tus compras.</p></div>");
            $('#form-register #id_dni').attr("placeholder", 'DNI');
            $('#form-register #id_password').attr("placeholder", 'Contraseña');
            $('#form-register #id_birthday').attr("placeholder", 'Fecha de Nacimiento (AAAA-MM-DD)');
            $('#div_id_musicals .control-label').text('Estilos Musicales');
            $('#id_influence').val(urlOrigin_influence);
            $('#id_event').val(urlOrigin_event);
            nextparams = location.search.split('next=')[1];
            registerFormAjaxSubmit('#form-register-modal-body form', '#form-register');
            loadValidatorDate();
        });
    });

    $('#form-facebook').submit(function () {
        localStorage.setItem('next', window.location.pathname);
    });
    /*
     * Otra lógica
     */


    $('.popups--mails').on('click', '.btn-submit', function (event) {
        $('.onEnv').addClass('fa-spinner-active');
        event.preventDefault();
        var csrftoken = document.getElementsByName("csrfmiddlewaretoken")[0].value;
        var form = $(this).closest('form');
        var today = new Date();
        var ss = today.getSeconds();
        var mn = today.getMinutes();
        var hh = today.getHours();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }
        today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + mn + ':' + ss;
        var data = {
            'name': form.find('.name').val(),
            'email2': form.find('.email2').val(),
            'email': form.find('.email').val(),
            'ticketObj': parseInt($(this).data('ticketvalue')),
            'sent_date': today
        };
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        });
        $.ajax({
            type: 'POST',
            url: '/api/sendTicket/',
            data: data,
            success: function (data) {
                $('.onEnv').removeClass('fa-spinner-active');
                swal("Buen Trabajo!", "Ticket enviado, ahora tu amigo ya tiene el ticket!", "success");
                window.location.href = "/profile/";
            },
            error: function (err) {
                swal("Oops...!", "Ocurrió un error en el envió, vuelva a intentarlo, revise bien sus datos.", "error");
                $('.modal').modal('hide');
            }
        });
    });
    $('.js--resent').on('click', function (event) {
        $('.onEnvFifth').addClass('fa-spinner-active');
        event.preventDefault();
        var csrftoken = document.getElementsByName("csrfmiddlewaretoken")[0].value;
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        });
        $.ajax({
            type: 'GET',
            url: '/api/resent/',
            success: function (data) {
                $('.onEnvFifth').removeClass('fa-spinner-active');
                swal("Correo reenviado!", "Revisa tu bandeja, para confirmar tu correo!", "success");
            },
            error: function (err) {
                swal("Oops...!", "Ocurrió un error en el envió, vuelva a intentarlo, revise bien sus datos.", "error");
                $('.modal').modal('hide');
            }
        });
    });


    $('.popups--mails__profile').on('click', '.btn-submit', function (event) {
        $('.onEnvTwo').addClass('fa-spinner-active');
        event.preventDefault();
        var csrftoken = document.getElementsByName("csrfmiddlewaretoken")[0].value;
        var form = $(this).closest('form');
        var today = new Date();
        var ss = today.getSeconds();
        var mn = today.getMinutes();
        var hh = today.getHours();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }
        today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + mn + ':' + ss;
        var data = {
            'name': form.find('.name_profile').val(),
            'email2': form.find('.email2_profile').val(),
            'email': form.find('.email_profile').val(),
            'ticketObj': parseInt($(this).data('ticketvalue')),
            'sent_date': today
        };
        $.ajaxSetup({
            beforeSend: function (xhr, settings) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        });
        $.ajax({
            type: 'POST',
            url: '/api/sendTicket/',
            data: data,
            success: function (data) {
                $('.onEnvTwo').removeClass('fa-spinner-active');
                swal("Buen Trabajo!", "Ticket enviado, ahora tu amigo ya tiene el ticket!", "success");
                window.location.href = "/profile/";
            },
            error: function (err) {
                swal("Oops...!", "Ocurrió un error en el envió, vuelva a intentarlo, revise bien sus datos.", "error");
                $('.modal').modal('hide');
            }
        });
    });

    // $('#ms').change(function () {
    //     console.log($(this).val());
    // }).multipleSelect({
    //     width: '100%'
    // });


    //VALIDADOR
    var vista = "";
    var element_id = "id_birthday";

    function loadValidatorDate() {
        document.getElementById("id_birthday").addEventListener('keyup', validar);
    }

    function validar() {
        var e = document.getElementById(element_id).value;
        var mens;
        var pos;
        if (e.length == 4 && isAnio(e.substring(0))) {
            e += '-';
            document.getElementById(element_id).value = e;
            //mens = "correcto";
            viewCorrect()
        }
        else if (e.length == 7 && isMes(e.substring(5, 7))) {
            e += '-';
            document.getElementById(element_id).value = e;
            vista = e;
            //mens = "correcto";
            viewCorrect()
        }
        else if (e.length == 10 && diasMes(e.substring(0, 4), e.substring(5, 7), e.substring(8, 10))) {
            //mens = "correcto";
            viewCorrect()
        }
        else {
            //mens = "error";
            viewCorrect()
        }
        //document.getElementById("mensaje").innerHTML = mens + " "+vista;
    }

    function viewCorrect() {
        texto = "";
        var element_id = "id_birthday";
        vista = document.getElementById(element_id).value;
        for (var i = 0; i < vista.length; i++) {
            if (vista[i] == '-')
                texto += '-';
            else
                texto += vista[i];
        }
        vista = texto;
    }

    function isMes(x) {
        if (x <= 12 && x > 0) {
            return true;
        }
        else {
            console.log(x);
            return false;
        }
    }

    function isAnio(x) {
        var a = new Date();
        if (x >= 1915 && x <= 2005) {
            return true;
        }
        else {
            return false;
        }
    }

    function diasMes(a, m, x) {
        var anio;
        var mes;
        var band = false;
        if (isAnio(a)) anio = a;
        else return band;
        if (isMes(m)) mes = m;
        else return false;
        switch (mes) {
            case '01':
            case '03':
            case '05':
            case '07':
            case '10':
            case '12':
                if (x > 0 && x <= 31)
                    band = true;
                else
                    band = false;
                break;
            case '04':
            case '06':
            case '09':
            case '11':
                if (x > 0 && x <= 30)
                    band = true;
                else
                    band = false;
                break;
            case '02':
                if (anio % 4 == 0) {
                    if (x > 0 && x <= 29)
                        band = true;
                    else
                        band = false;
                }
                else {
                    if (x > 0 && x <= 28)
                        band = true;
                    else
                        band = false;
                }
                break;
        }
        return band;
    }


    /* CITIES - MONTHS ==> FILTERS  */

    var path_origin = {};
    var path_complete;
    $('#month-filters li').on('click', function () {
        month = $(this).data('value');
        path_origin.month = month;
    });
    $('#cities-filters li').on('click', function () {
        city = $(this).data('value');
        path_origin.city = city;
    });

    $('#search_evn').on('click', function (e) {
        e.preventDefault(); //href prevenir
        if (path_origin.month && path_origin.city) {
            path_complete = window.location.origin + '/eventos/month/' + path_origin.month + '/city/' + path_origin.city;
        } else if (path_origin.city) {
            path_complete = window.location.origin + '/eventos/city/' + path_origin.city;
        } else {
            path_complete = window.location.origin + '/eventos/month/' + path_origin.month;
        }
        window.location.href = path_complete;
    });

    $('.select').on('click', '.placeholder', function () {
        var parent = $(this).closest('.select');
        if (!parent.hasClass('is-open')) {
            parent.addClass('is-open');
            $('.select.is-open').not(parent).removeClass('is-open');
        } else {
            parent.removeClass('is-open');
        }
    }).on('click', 'ul>li', function () {
        var parent = $(this).closest('.select');
        parent.removeClass('is-open').find('.placeholder').text($(this).text());
        parent.find('input[type=hidden]').attr('value', $(this).attr('data-value'));
    });

    var register = false;
    $('#form-login').on('hidden.bs.modal', function (e) {
        if (register) {
            $("#form-register").modal('show');
            register = false;
        }

    });
    $('#btn-register').on('click', function () {
        register = true;
        $("#form-login").modal('hide');
        //$('body').addClass('modal-open');
    });
    /* END CITIES - MONTHS ==> FILTERS  */

    var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

    /*copyTextareaBtn.addEventListener('click', function (event) {
     var copyTextarea = document.querySelector('.js-copytextarea');
     copyTextarea.select();

     try {
     var successful = document.execCommand('copy');
     var msg = successful ? 'successful' : 'unsuccessful';
     console.log('Copying text command was ' + msg);
     } catch (err) {
     console.log('Oops, unable to copy');
     }
     });*/
});