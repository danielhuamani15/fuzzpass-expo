// var eventsApp = angular.module('events', ['infinite-scroll']).config(['$httpProvider', '$interpolateProvider', function ($httpProvider, $interpolateProvider) {
//     function csrfSafeMethod(method) {
//         return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
//     }

//     function sameOrigin(url) {
//         var host = document.location.host; // host + port
//         var protocol = document.location.protocol;
//         var sr_origin = '//' + host;
//         var origin = protocol + sr_origin;

//         return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
//             (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') || !(/^(\/\/|https:).*/.test(url));
//     }

//     $.ajaxSetup({
//         beforeSend: function (xhr, settings) {
//             if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
//                 xhr.setRequestHeader("X-CSRFToken", $('meta[name=_csrf]').attr('content'));
//             }
//         }
//     });
//     $httpProvider.defaults.xsrfCookieName = 'csrftoken';
//     $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
//     $interpolateProvider.startSymbol('[[');
//     $interpolateProvider.endSymbol(']]');
// }]).run(['$rootScope', function ($rootScope) {
// }]);

// eventsApp.controller('eventsAllController', ['$sce', '$scope', '$http', function ($sce, $scope, $http) {
//     $scope.currentDate = new Date();
//     $scope.loading = false;
//     $scope.filteredForSider = [];
//     $scope.date = new Date();
//     $scope.events_manager = {
//         loading_posts: false,
//         initial: function () {
//             self.host = document.location.host; // host + port
//             self.protocol = document.location.protocol;
//             self.sr_origin = '//' + host;
//             self.origin = protocol + sr_origin + '/';
//         },
//         load: function () {
//             if (!$scope.events_manager.loading_events) {
//                 $scope.events_manager.loading_events = true;
//                 if ($scope.page != null) {
//                     var url_link = $scope.page;
//                     $http({
//                         method: 'GET',
//                         url: url_link
//                     })
//                         .then(function (response) {
//                             var data = response.data;

//                             $scope.page = data.next;
//                             $scope.items = $scope.items || [];
//                             $scope.items.push.apply($scope.items, data.results);
//                             $scope.items.forEach(function (item) {
//                                 if (new Date(item.end_date) > $scope.date) {
//                                     $scope.filteredForSider.push(item);
//                                 }
//                                 item.end_date = new Date(item.end_date);
//                                 item.presale_end_date = new Date(item.presale_end_date);
//                                 item.map = $sce.trustAsResourceUrl("https://maps.google.com/maps?q=" + item.location.latitude + "," + item.location.longitude + "&z=15&output=embed")
//                             });
//                             $scope.events_manager.loading_events = false;
//                         });
//                 } else {
//                     $scope.events_manager.loading_events = false;
//                 }
//             }
//             else {
//             }
//         },
//         do_like: function (event, comment) {
//             $http({
//                 method: 'POST',
//                 url: self.origin + 'api/events/' + event.id + "/wish/"
//             }).then(function (response) {
//                 event.is_wish = true;
//             });
//         },
//         dislike: function (event) {
//             $http({
//                 method: 'POST',
//                 url: self.origin + 'api/events/' + event.id + "/unwish/"
//             }).then(function (response) {
//                 event.is_wish = false;
//             });
//         },
//         filter: function () {

//             if ($scope.month && $scope.city) {
//                 $scope.page = self.origin + 'api/events/month/' + $scope.month + '/city/' + $scope.city;

//             } else if ($scope.city) {
//                 $scope.page = self.origin + 'api/events/city/' + $scope.city;
//             } else {
//                 $scope.page = self.origin + 'api/events/month/' + $scope.month;
//             }
//             if ($scope.page != null) {
//                 var url_link = $scope.page;
//                 $http({
//                     method: 'GET',
//                     url: url_link
//                 }).then(function (response) {
//                     var data = response.data;

//                     $scope.page = data.next;
//                     $scope.items = $scope.items || [];
//                     $scope.items.push.apply($scope.items, data.results);
//                     $scope.items.forEach(function (item) {
//                         if (new Date(item.end_date) > $scope.date) {
//                             $scope.filteredForSider.push(item);
//                         }
//                         item.end_date = new Date(item.end_date);

//                     });
//                     if ($scope.items.length == 0) {
//                         console.log("no hay coincidencias");
//                         $scope.textInfo = 'No se encontró ninguna coincidencia';
//                         $scope.noFoundMsg = true
//                     } else {
//                         $scope.items = data.results;
//                         console.log("entro");
//                         console.log($scope.items);
//                     }
//                     $scope.events_manager.loading_events = true;
//                 });
//             } else {
//                 $scope.events_manager.loading_events = false;
//             }
//         }
//     };
//     $scope.events_manager.initial();
//     $scope.page = self.origin + 'api/events/';
//     $scope.events_manager.load();

//     $('#month-filters li').on('click', function () {
//         $scope.month = $(this).data('value');
//     });
//     $('#cities-filters li').on('click', function () {
//         $scope.city = $(this).data('value');
//     });
// }]);


// eventsApp.controller('producerEventsController', ['$scope', '$http', function ($scope, $http) {
//     $scope.currentDate = new Date();
//     $scope.loading = false;
//     $scope.date = new Date();
//     $scope.events_manager = {
//         loading_posts: false,
//         initial: function () {
//             self.host = document.location.host; // host + port
//             self.protocol = document.location.protocol;
//             self.sr_origin = '//' + host;
//             self.origin = protocol + sr_origin + '/';
//         },
//         load: function () {
//             if (!$scope.events_manager.loading_events) {
//                 $scope.events_manager.loading_events = true;
//                 if ($scope.page != null) {
//                     var url_link = $scope.page;

//                     $http({
//                         method: 'GET',
//                         url: url_link
//                     })
//                         .then(function (response) {
//                             var data = response.data;

//                             $scope.page = data.next;
//                             $scope.events = $scope.events || [];
//                             $scope.events.push.apply($scope.events, data.results);
//                             $scope.events.forEach(function (event) {
//                                 event.end_date = new Date(event.end_date);
//                             });
//                             $scope.events_manager.loading_events = false;
//                         });
//                 } else {
//                     $scope.events_manager.loading_events = false;
//                 }
//             }
//         },
//         do_like: function (event, comment) {
//             $http({
//                 method: 'POST',
//                 url: self.origin + 'api/events/' + event.id + "/wish/"
//             }).then(function (response) {
//                 event.is_wish = true;
//             });
//         },
//         dislike: function (event) {
//             $http({
//                 method: 'POST',
//                 url: self.origin + 'api/events/' + event.id + "/unwish/"
//             }).then(function (response) {
//                 event.is_wish = false;
//             });
//         }
//     };
//     $scope.events_manager.initial();
//     $scope.producer = {
//         id: window.producer_id
//     };
//     $scope.page = self.origin + 'api/productoras/' + $scope.producer.id + '/events/';

//     $scope.events_manager.load();
// }]);

// eventsApp.directive('backImg', function () {
//     return function (scope, element, attrs) {
//         var url = attrs.backImg;
//         element.css({
//             'background-image': 'url(' + url + ')',
//             'background-size': 'cover'
//         });
//     };
// });
// eventsApp.filter('capitalize', function () {
//     return function (input) {
//         return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
//     }
// });

