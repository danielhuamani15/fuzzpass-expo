'use strict';
var __baseDist = 'static_dist/';
var __baseSource = 'static/';

var modulo = {
    paths: {
        styles   : [
            __baseSource + '/styles/**/*.scss'
        ],
        scripts  : [
            __baseSource + '/scripts/**/*.js'
        ],
        images   : [
            __baseSource + 'images/**/*.{png,jpg}'
        ],
        fonts    : [
            __baseSource + '/fonts/**/*.{ttf,otf,woff}'
        ]
    },
    dist: {
        styles   : __baseDist + 'styles',
        images   : __baseDist + 'images',
        scripts  : __baseDist + 'scripts',
        fonts    : __baseDist + 'fonts'
    },


    browser     : ['google-chrome'], //firefox
    port        : 8005,              /*error port: 6000*/
    handleError : function(error) {
        console.log(error.toString());
        this.emit('end');
    },
    reload      : function(server, browserSync, util) {
        if (server) {
            return browserSync.reload({ stream: true })
        }
        return util.noop();
    }
};

module.exports = modulo;
