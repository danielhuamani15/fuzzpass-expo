from apps.seats.models import *

# Update the ids accordingly
tn_event = Event.objects.get(pk=257)
tn_event.zones.all().values_list('id')

platea_zone = Zone.objects.get(id=396)
piso2_zone  = Zone.objects.get(id=397)
piso3_zone  = Zone.objects.get(id=398)
piso4_zone  = Zone.objects.get(id=399)

print(platea_zone)
print(piso2_zone)
print(piso3_zone)
print(piso4_zone)

event_location = EventLocation.objects.create(name='Teatro Nacional')
event_location.events.add(tn_event)

platea_section = Section.objects.create(
    location=event_location, name='PLATEA', template_name='seats/national_theater_1.html')
platea_section.zones.add(platea_zone)

piso2_section = Section.objects.create(
    location=event_location, name='PISO 2', template_name='seats/national_theater_2.html')
piso2_section.zones.add(piso2_zone)

piso3_section = Section.objects.create(
    location=event_location, name='PISO 3', template_name='seats/national_theater_3.html')
piso3_section.zones.add(piso3_zone)

piso4_section = Section.objects.create(
    location=event_location, name='PISO 4', template_name='seats/national_theater_4.html')
piso4_section.zones.add(piso4_zone)


platea_grid = {
    'A': 23,
    'B': 30,
    'C': 29,
    'D': 28,
    'E': 29,
    'F': 28,
    'G': 31,
    'H': 30,
    'I': 31,
    'J': 30,
    'K': 31,
    'L': 28,
    'M': 23,
    'N': 16,
    'O': 7,
    'P': 62,
    'Q': 63,
    'R': 40,
    'S': 42,
    'T': 44,
    'U': 38,
    'AA': 26,
    'BB': 26,
    'CC': 26,
    'DD': 18,
    'PALCO_LEFT': 4,
    'PALCO_RIGHT': 4
}

piso2_grid = {
    'A': 59,
    'B': 66,
    'C': 43,
    'D': 42,
    'PALCO_LEFT': 4,
    'PALCO_RIGH': 4
}

piso3_grid = {
    'A': 64,
    'B': 67,
    'C': 40,
    'D': 41,
    'PALCO_1_LEFT': 4,
    'PALCO_1_RIGHT': 4,
    'PALCO_2_LEFT': 4,
    'PALCO_2_RIGHT': 4
}

piso4_grid = {
    'A': 64,
    'B': 39,
    'C': 40,
    'D': 39,
    'E': 24
}

def create_seats_from_grid(grid, zone, section):
    for letter, quantity in grid.items():
        for number in range(1, quantity + 1):
            seat = Seat.objects.create(section=section, letter=letter, number=number)
            ZoneSeat.objects.create(zone=zone, seat=seat)

create_seats_from_grid(platea_grid, platea_zone, platea_section)
create_seats_from_grid(piso2_grid, piso2_zone, piso2_section)
create_seats_from_grid(piso3_grid, piso3_zone, piso3_section)
create_seats_from_grid(piso4_grid, piso4_zone, piso4_section)