var gulp        = require('gulp');
var sass = require('gulp-sass');
var minimist = require('minimist');
var gutil = require('gulp-util');
var browserSync = require('browser-sync');
var config = require('./const.js');
var reload      = browserSync.reload;
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var handleError = config.handleError;
var environment = process.env.NODE_ENV;
var __baseDist = 'static_dist/';
var __baseSource = './static/';

var paths = {
    src: {
        styles: [
            __baseSource + 'styles/**/*.scss'
        ],
        scripts: [
            __baseSource + 'bower_components/jquery/dist/jquery.js',
            __baseSource + 'scripts/modernizr.js',
            __baseSource + 'scripts/culqi.js',
            __baseSource + 'scripts/card.js',
            __baseSource + 'scripts/csrf.js',
            __baseSource + 'scripts/app.js',
            __baseSource + 'bower_components/sweetalert/dist/sweetalert.min.js',
            __baseSource + 'bower_components/jquery.countdown/dist/jquery.countdown.min.js',
            // __baseSource + 'bower_components/angular/angular.js',
            // __baseSource + 'bower_components/ngInfiniteScroll/build/ng-infinite-scroll.js',
            __baseSource + 'scripts/**/*.js',
            // __baseSource + 'bower_components/angular-locale_es-es/angular-locale_es-es.js',
            __baseSource + 'bower_components/bootstrap/dist/js/bootstrap.js',
            __baseSource + 'bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js',
            __baseSource + 'bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.es.min.js'
        ],
        images: [
            __baseSource + 'images/**/*.{png,jpg,ico,gif}'
        ],
        fonts: [
            __baseSource + 'fonts/**/*.{ttf,otf,woff,woff2}'
        ],
        templates: [
           'partials/**/*.html'
        ]
    },
    dist: {
        styles: __baseDist + 'styles',
        images: __baseDist + 'images',
        scripts: __baseDist + 'scripts',
        fonts: __baseDist + 'fonts'
    }
};


gulp.task('images', function () {
    return gulp
        .src(paths.src.images)
        .pipe(gulp.dest(paths.dist.images))
        .pipe(reload({stream: true}))
});

gulp.task('fonts', function () {
    return gulp
        .src(paths.src.fonts)
        .pipe(gulp.dest(paths.dist.fonts))
        .pipe(reload({stream: true}))
});

gulp.task('script', function () {
    return gulp
        .src(paths.src.scripts)
        .pipe(sourcemaps.init())
        .on('error', gutil.log)
        .pipe(concat("all.min.js"))
        .pipe(environment == 'production' ? uglify() : gutil.noop())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.dist.scripts))
        .pipe(reload({stream: true}))
});

gulp.task('style', function () {
    return gulp
        .src(paths.src.styles)
        .pipe(sourcemaps.init())
        .pipe(environment == 'production' ? sass({outputStyle: 'compressed'}) : sass()).on('error', handleError)
        .pipe(concat("all.min.css"))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.dist.styles))
        .pipe(reload({stream: true}))
});

gulp.task('myhtml', function() {
    return gulp
        .src(paths.src.templates)
        .pipe(reload({stream: true}))
});

gulp.task('build', ['style', 'images', 'fonts', 'script']);

gulp.task('watch', function () {
    gulp.watch(paths.src.templates, ['myhtml']);
    gulp.watch(paths.src.images, ['images']);
    gulp.watch(paths.src.fonts, ['fonts']);
    gulp.watch(paths.src.scripts, ['script']);
    gulp.watch(paths.src.styles, ['style']);
});
gulp.task('default', ['build','watch'], function() {
    browserSync.init({
        notify: false,
        browser: config.browser,
        proxy: "localhost:8004"
    });
});
if (environment == 'development'){
    gulp.task('default', ['build','watch'], function() {
        browserSync.init({
            notify: false,
            browser: config.browser,
            proxy: "localhost:8004"
        });
    });
}
else{
    if(environment == 'production'){
        gulp.task('build', ['style', 'images', 'fonts', 'script']);
        gulp.task('default', ['build','watch']);
    }
}
