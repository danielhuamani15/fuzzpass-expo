import os
from hashids import Hashids
from django.core.urlresolvers import reverse_lazy

BASE_DIR = os.path.dirname(os.path.dirname(
    os.path.dirname(os.path.abspath(__file__))))
SECRET_KEY = '(qroa9*0z5w+9e*txn4duh!sq)z&6emf4nz4+k%i-m9s_&mlx^'

INSTALLED_APPS = [
    'jet.dashboard',
    'jet',
    'django.contrib.admin',
    'django.contrib.sites',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'social.apps.django_app.default',
    'easy_thumbnails',
    'crispy_forms',
    'corsheaders',
    'rest_framework',
    'apps.payment',
    'import_export',
    'apps.web',
    'apps.accounts',
    'apps.section',
    'apps.influences',
    'django.contrib.gis',
    'rest_framework.authtoken',
    'apps.companies',
    'apps.venues',
    'apps.seats',
    'django_crontab',
    'django.contrib.humanize'
]

MIDDLEWARE_CLASSES = [
    'corsheaders.middleware.CorsMiddleware',
    'apps.core.middleware.MySubdomainMiddleware',
    'apps.web.middleware.NotificationMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
]

ROOT_URLCONF = 'fuzzpass.urls'

HASHIDS = Hashids(salt=SECRET_KEY, min_length=8)

CULQI_KEY = 'eMKCtUeUjh9muKp41UF1MAdTVcSxsttxVfHmgpIa6H4='
CULQI_COMERCE_CODE = 'kRfPKwPTfWc1'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'partials')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'fuzzpass.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'accounts.User'

LANGUAGE_CODE = 'es-PE'

TIME_ZONE = 'America/Lima'

USE_I18N = True
USE_L10N = True
EMAIL_USE_TLS = True
EMAIL_HOST = 'mail.festivaldelima.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'reservas@festivaldelima.com'
EMAIL_HOST_PASSWORD = '14*nGP2IHx7w'

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
STATIC_URL = '/static/'

#STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.CachedStaticFilesStorage'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static_dist'),
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

LOGIN_URL = reverse_lazy('accounts:login-signup')
LOGIN_REDIRECT_URL = reverse_lazy('accounts:profile')

CRISPY_TEMPLATE_PACK = 'bootstrap3'

THUMBNAIL_ALIASES = {
    '': {
        'small': {'size': (600, 350), 'crop': True},
        'normal_full': {'size': (900, 482), 'sharpen': True},
    },
}
REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.TokenAuthentication',
    ),
}

AUTHENTICATION_BACKENDS = (
    'social.backends.facebook.FacebookAppOAuth2',
    'social.backends.facebook.FacebookOAuth2',
    'social.backends.twitter.TwitterOAuth',
    'social.backends.google.GoogleOAuth2',
    'django.contrib.auth.backends.ModelBackend',
)

SOCIAL_AUTH_FACEBOOK_KEY = '1039760222746515'
SOCIAL_AUTH_FACEBOOK_SECRET = '58005a4dd0ba96bf4f237f20a742485a'

SOCIAL_AUTH_PIPELINE = (
    'social.pipeline.social_auth.social_details',
    'social.pipeline.social_auth.social_uid',
    'social.pipeline.social_auth.auth_allowed',
    'social.pipeline.social_auth.social_user',
    'social.pipeline.user.get_username',
    'social.pipeline.social_auth.associate_by_email',  # <--- enable this one
    'social.pipeline.user.create_user',
    'social.pipeline.social_auth.associate_user',
    'social.pipeline.social_auth.load_extra_data',
    'social.pipeline.user.user_details',
    'apps.accounts.pipeline.save_profile_picture',
)

SOCIAL_AUTH_USER_MODEL = 'accounts.User'

SOCIAL_AUTH_USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_FACEBOOK_PROFILE_EXTRA_PARAMS = {
    'fields': 'id,name,email',
}

SOCIAL_AUTH_PROTECTED_USER_FIELDS = ['email', 'first_name', 'last_name']

SOCIAL_AUTH_USER_FIELDS = ['email', 'first_name', 'last_name']

SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

FACEBOOK_EXTENDED_PERMISSIONS = ['email']

DEFAULT_FROM_EMAIL = "info@fuzzpass.com"

# ADMINS = (

# )

JET_SIDE_MENU_COMPACT = True
JET_DEFAULT_THEME = 'light-gray'

# CELERY SETTINGS
BROKER_URL = 'redis://localhost:6379/0'

# CELERY_ACCEPT_CONTENT = ['json']
# CELERY_TASK_SERIALIZER = 'json'
# CELERY_RESULT_SERIALIZER = 'json'

CULQI_PUBLIC_KEY = 'pk_test_IQ5IEP7b1UzZIyah'
CULQI_SECRET_KEY = 'sk_test_TunapCBKpro9Vrkr'

CRONJOBS = [
    ('0 */2 * * *', 'apps.seats.jobs.free_reserved_seats_job',
     '>> /tmp/free_reserved_seats_jobs.log'),
]
CRONTAB_COMMAND_SUFFIX = '2>&1'


# DATE_FORMAT = '%d/%m/%Y'
# DATE_INPUT_FORMATS = [
#     '%d/%m/%Y', '%d-%m-%Y',              # '25-10-2006', '25/10/2006'
#     '%d/%m/%y', '%d/%m/%Y',             # '25/10/06, #'25/10/2006'
#     '%b %d %Y', '%b %d, %Y',            # 'Oct 25 2006', 'Oct 25, 2006'
#     '%d %b %Y', '%d %b, %Y',            # '25 Oct 2006', '25 Oct, 2006'
#     '%B %d %Y', '%B %d, %Y',            # 'October 25 2006', 'October 25, 2006'
#     '%d %B %Y', '%d %B, %Y',            # '25 October 2006', '25 October, 2006'
# ]
