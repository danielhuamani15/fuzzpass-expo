from .development import *  # noqa

MEDIA_ROOT = '/data/media/'
STATIC_ROOT = '/data/static/'

DATABASES['default']['HOST'] = 'db'
