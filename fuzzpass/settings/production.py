from .base import *
import raven


DEBUG = True
SITE_URL = '165.227.127.113'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'USER': 'fp',
        'PASSWORD': 'fp',
        'HOST': 'localhost',
        'PORT': '5432',
        'NAME': 'fp',
    }
}

ALLOWED_HOSTS = [
    '165.227.127.113'
]

# INSTALLED_APPS = [
#     'raven.contrib.django.raven_compat',
# ] + INSTALLED_APPS

# RAVEN_CONFIG = {
#     'dsn': 'http://6986170ba1c541ee861aa1ad250c9958:f15bb236653c4a7ab1f6141e86fdb3a7@sentry.fuzzpass.com/4',
# }


# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
# SESSION_COOKIE_SECURE = True
# CSRF_COOKIE_SECURE = True

# CULQI_PUBLIC_KEY = 'pk_live_NsTxzbM50LOlPELn'
# CULQI_SECRET_KEY = 'sk_live_YJp3N3kFX6Dl0PFJ'

# CRONJOBS = [
#     ('*/30 * * * *', 'apps.seats.jobs.free_reserved_seats_job', '>> /tmp/free_reserved_seats_jobs.log'),
# ]

# CRONTAB_COMMAND_SUFFIX = '2>&1'
# CRONTAB_DJANGO_SETTINGS_MODULE = 'fuzzpass.settings.production'

MEDIA_ROOT = '/home/dev/media'  # noqa

MEDIA_URL = '/media/'
STATIC_ROOT = '/home/dev/static/'

STATIC_URL = '/static/'
URL_SITE = 'http://reservas.festivaldelima.com'
