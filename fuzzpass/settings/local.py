from .base import *

DEBUG = True

INSTALLED_APPS += ('debug_toolbar',)

db_type = 'postgres'

if db_type == 'sqlite':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        }
    }
elif db_type == 'postgres':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'fuzzpass',
            'USER': 'daniel',
            'PASSWORD': 'daniel123',
            'HOST': 'localhost'
        }
    }

URL_SITE = 'http://165.227.127.113'