from .base import *

DEBUG = True

INSTALLED_APPS += ('debug_toolbar', 'django_extensions')

CORS_ORIGIN_ALLOW_ALL = True

SITE_ID = 1

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'fuzzpass_expo',
        'USER': 'daniel',
        'PASSWORD': 'daniel123',
        'HOST': 'localhost',
        'PORT': '5432'
    }
}

ALLOWED_HOSTS = [
    '*'
]

STATIC_ROOT = os.path.join(BASE_DIR, 'static')