from .base import *

DEBUG = True
SITE_ID = 6

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'USER': 'fuzzpass',
        'PASSWORD': 'fuzzpass',
        'HOST': 'localhost',
        'PORT': '5432',
        'NAME': 'fuzzpass',
    }
}

ALLOWED_HOSTS = [
    '159.65.246.207',
    'staging.fuzzpass.com',
    'localhost'
]

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
