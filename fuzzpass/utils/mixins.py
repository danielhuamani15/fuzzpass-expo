from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin


class UidMixin:
    @property
    def uid(self):
        return settings.HASHIDS.encode(self.id)


class ConfirmMailRequireMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.confirm:
            return self.handle_no_permission()

        return super(ConfirmMailRequireMixin, self).dispatch(request, *args, **kwargs)
