from django.conf.urls import url, include, static
from django.contrib import admin
from django.conf import settings

admin.site.site_header = 'Fuzzpass'

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^admin/', admin.site.urls),
    url(r'^', include('apps.web.urls', namespace='webs', app_name='webs')),
    url(r'^', include('apps.accounts.urls', namespace='accounts', app_name='accounts')),
    url(r'^', include('apps.payment.urls', namespace='payments', app_name='payments')),
    url(r'^', include('apps.influences.urls', namespace='influences', app_name='influences')),
    # url(r'^', include('apps.seats.urls', namespace='seats', app_name='seats')),
    url('', include('social.apps.django_app.urls', namespace='social'))
]

if settings.DEBUG:
    urlpatterns += static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
