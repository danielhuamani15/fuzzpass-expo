from django.db import models
from tinymce.models import HTMLField


class AboutUs(models.Model):
    about = models.TextField(verbose_name='Acerca de nosotros')
    team_image = models.ImageField(upload_to='team', verbose_name='Imagen de equipo')
    team_description = models.TextField(verbose_name='Descripción del equipo')

    def __str__(self):
        return 'Acerca de Nosotros'

    class Meta:
        verbose_name_plural = 'Acerca de Nosotros'


class HowWorks(models.Model):
    banner = models.ImageField(upload_to='banner')
    benefits_content = models.TextField(verbose_name='Principales Beneficios')

    class Meta:
        verbose_name_plural = 'Como Funciona'

    def __str__(self):
        return 'Como Funciona'


class Benefits(models.Model):
    img = models.ImageField(upload_to='benefits', verbose_name='Imagen')
    tittle = models.CharField(max_length=100, verbose_name='Título', help_text='Llenar Aquí el título')
    content = models.TextField(verbose_name='Contenido', help_text='llenar aquí el contenido')
    how_works = models.ForeignKey(HowWorks, related_name='howworks', verbose_name='Beneficios')

    class Meta:
        verbose_name = 'Beneficio'


class Terms(models.Model):
    description = models.TextField(verbose_name="Descripción", help_text="Ingrese la descripción")

    class Meta:
        verbose_name_plural = 'Terminos y Condiciones'

    def __str__(self):
        return 'Términos y Condiciones'


class Politics(models.Model):
    description = models.TextField(verbose_name="Descripción", help_text="Ingrese la descripción")

    class Meta:
        verbose_name_plural = 'Politicas de Privacidad'

    def __str__(self):
        return 'Politicas de Privacidad'
