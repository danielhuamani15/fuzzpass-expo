from django.contrib import admin
from django.db import models
from django import forms
from .models import HowWorks, AboutUs, Benefits, Terms, Politics


class AdminTerms(admin.ModelAdmin):
    formfield_overrides = {models.TextField: {'widget': forms.Textarea(attrs={'class': 'ckeditor'})},}

    class Media:
        js = ('https://cdn.ckeditor.com/4.5.10/basic/ckeditor.js',)


class AdminPolitics(admin.ModelAdmin):
    formfield_overrides = {models.TextField: {'widget': forms.Textarea(attrs={'class': 'ckeditor'})},}

    class Media:
        js = ('https://cdn.ckeditor.com/4.5.10/basic/ckeditor.js',)


class AdminAboutUs(admin.ModelAdmin):
    formfield_overrides = {models.TextField: {'widget': forms.Textarea(attrs={'class': 'ckeditor'})},}

    class Media:
        js = ('https://cdn.ckeditor.com/4.5.10/basic/ckeditor.js',)


class AdminBenefitsInline(admin.TabularInline):
    model = Benefits
    extra = 1

    formfield_overrides = {models.TextField: {'widget': forms.Textarea(attrs={'class': 'ckeditor'})},}

    class Media:
        js = ('https://cdn.ckeditor.com/4.5.10/basic/ckeditor.js',)


class HowWorksAdmin(admin.ModelAdmin):
    formfield_overrides = {models.TextField: {'widget': forms.Textarea(attrs={'class': 'ckeditor'})},}

    class Media:
        js = ('https://cdn.ckeditor.com/4.5.10/basic/ckeditor.js',)

    inlines = [AdminBenefitsInline]


admin.site.register(HowWorks, HowWorksAdmin)
# admin.site.register(TermsAndConditions, TermsAdmin)
admin.site.register(Terms, AdminTerms)
admin.site.register(AboutUs, AdminAboutUs)
admin.site.register(Politics, AdminPolitics)
