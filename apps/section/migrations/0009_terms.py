# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-09-06 02:31
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('section', '0008_auto_20160902_0200'),
    ]

    operations = [
        migrations.CreateModel(
            name='Terms',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField(help_text='Descripcion', verbose_name='Ingrese la descripcion')),
            ],
        ),
    ]
