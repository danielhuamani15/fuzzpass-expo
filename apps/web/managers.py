from django.db import models
from django.utils import timezone

class EventQueryset(models.query.QuerySet):

    def public(self, subdomain=None):
        if subdomain != 'www' and subdomain != 'staging' and subdomain:
            return self.filter(
                status=True, private=False,
                venue__company__subdomain=subdomain,
                venue__company__active=True,
                venue__company__whitelabel=True
            )
        return self.filter(status=True, private=False)

    def private(self, subdomain=None):
        if subdomain != 'www' and subdomain != 'staging' and subdomain:
            return self.filter(
                status=True, private=True,
                venue__company__subdomain=subdomain,
                venue__company__active=True,
                venue__company__whitelabel=True
            )
        return self.filter(status=True, private=True)

    def all(self, subdomain=None):
        if subdomain != 'www' and subdomain != 'staging' and subdomain:
            return self.filter(
                status=True, private=False,
                venue__company__subdomain=subdomain,
                venue__company__active=True,
                venue__company__whitelabel=True
            )
        return self.filter(status=True)

    def current_public(self, subdomain=None):
        today = timezone.now().today()
        print(today, 'today')
        return self.public(subdomain).filter(start_date__gte=today)


class EventManager(models.Manager):

    def get_queryset(self):
        return EventQueryset(self.model, using=self._db)

    def public(self, subdomain=None):
        return self.get_queryset().public(subdomain)

    def private(self, subdomain=None):
        return self.get_queryset().private(subdomain)

    def all(self, subdomain=None):
        return self.get_queryset().all(subdomain)

    def current_public(self, subdomain=None):
        return self.get_queryset().current_public(subdomain)
