# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-17 20:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0013_auto_20160617_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='band_description',
            field=models.TextField(blank=True, help_text='Ingrese la descripción de las bandas', max_length=1024, null=True, verbose_name='Acerca de las bandas'),
        ),
        migrations.AlterField(
            model_name='event',
            name='description',
            field=models.TextField(help_text='Ingrese la descripción del evento', max_length=1224, verbose_name='Descripción del evento'),
        ),
    ]
