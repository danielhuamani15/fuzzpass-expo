# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-07-14 16:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0030_auto_20160714_1041'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='social',
        ),
        migrations.AddField(
            model_name='social',
            name='events',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='socials', to='web.Event'),
        ),
    ]
