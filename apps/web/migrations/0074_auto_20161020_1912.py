# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-10-20 19:12
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0073_auto_20161020_1657'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 10, 21, 19, 12, 25, 665885), verbose_name='Fecha de fin'),
        ),
    ]
