# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-15 17:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0006_auto_20160615_1634'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='band',
        ),
        migrations.RemoveField(
            model_name='event',
            name='zone',
        ),
        migrations.AddField(
            model_name='event',
            name='band_description',
            field=models.TextField(blank=True, help_text='Ingrese la descripción de las bandas', max_length=700, null=True, verbose_name='Acerca de las bandas'),
        ),
        migrations.AddField(
            model_name='event',
            name='zones',
            field=models.ManyToManyField(related_name='events', to='web.Zone'),
        ),
        migrations.DeleteModel(
            name='Band',
        ),
    ]
