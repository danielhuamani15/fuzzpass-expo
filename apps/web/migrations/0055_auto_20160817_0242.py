# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-17 07:42
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0054_auto_20160817_0240'),
    ]

    operations = [
        migrations.AlterField(
            model_name='zone',
            name='name',
            field=models.CharField(default='Zona General', help_text='Ingrese el nombre de la zona', max_length=16, verbose_name='Nombre de la zona'),
        ),
        migrations.AlterField(
            model_name='zone',
            name='order',
            field=models.PositiveSmallIntegerField(blank=True, default=1, null=True, verbose_name='Prioridad'),
        ),
    ]
