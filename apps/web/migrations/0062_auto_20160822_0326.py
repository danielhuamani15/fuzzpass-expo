# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-22 08:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0061_videoevent'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='videoevent',
            options={'verbose_name': 'Video', 'verbose_name_plural': 'Videos'},
        ),
    ]
