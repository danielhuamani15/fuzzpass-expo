# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-08-19 06:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0058_event_cover_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='ticket_cover',
            field=models.ImageField(blank=True, help_text='Ingrese la portada para el ticket', null=True, upload_to='Ticket/covers', verbose_name='Portada de ticket'),
        ),
    ]
