# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-09-13 15:48
from __future__ import unicode_literals

import apps.web.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0066_event_color'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='color',
            field=apps.web.models.ColorField(blank=True, default='', max_length=10, verbose_name='Fondo Ticket'),
        ),
    ]
