# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-02-18 17:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0090_auto_20170115_1400'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='private',
            field=models.BooleanField(default=False, verbose_name='¿Evento privado?'),
        ),
    ]
