# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2017-01-04 16:02
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0085_producer_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='presale_end_date',
            field=models.DateTimeField(blank=True, null=True, verbose_name='Fecha de final de preventa'),
        ),
    ]
