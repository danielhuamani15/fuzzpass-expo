# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-10-20 12:39
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0069_event_end_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='end_date',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name='Fecha de fin'),
        ),
        migrations.AlterField(
            model_name='event',
            name='start_date',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name='Fecha de inicio'),
        ),
    ]
