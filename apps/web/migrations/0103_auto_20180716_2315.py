# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2018-07-16 23:15
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0102_zoneticket'),
    ]

    operations = [
        migrations.RenameField(
            model_name='zoneticket',
            old_name='is_free',
            new_name='is_bought',
        ),
    ]
