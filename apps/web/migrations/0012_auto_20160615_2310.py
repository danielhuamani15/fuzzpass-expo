# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-15 23:10
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0011_auto_20160615_2121'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(blank=True, geography=True, null=True, srid=4326, verbose_name='Location'),
        ),
    ]
