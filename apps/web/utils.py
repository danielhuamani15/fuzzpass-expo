import datetime
import json

from io import BytesIO
import qrcode

from django.core.files.uploadedfile import InMemoryUploadedFile

from apps.accounts.models import User
from apps.payment.models import Sale, Ticket
from apps.payment.tasks import send_email_sale
from apps.web.models import Event, EventUserRegistration


def generate_zone_ticket(zone_ticket, client):
    zone_ticket = zone_ticket.first()

    ticket = Ticket()
    ticket.owner_id = client.id
    event = zone_ticket.zone.events
    ticket.event = event
    # if source_person_obj:
    #     ticket.influence = source_person_obj

    # if seat is not None:
    #     ticket.seat = seat[0]
    #     ticket.seat_section = seat[1]
    # ticket.sales = sale_obj
    ticket.seat_section = zone_ticket.zone.name
    ticket.zone = zone_ticket.zone
    ticket.price = float(0)
    ticket.save()

    ticket.code = ticket.get_code()

    sale_obj = Sale()
    sale_obj.amount = 0
    sale_obj.description = 'Invitación generada desde Fuzz Pass'
    sale_obj.money = event.currency
    sale_obj.client_id = client.id
    sale_obj.status = 'v'
    sale_obj.source_type = 'f'
    if client:
        sale_obj.source_person = client
    sale_obj.save()
    EventUserRegistration.objects.create(user=client, event=event)
    # data = {
    #     'seat': str(seat),
    #     'ticker': ticket.code
    # }
    # LogsEvent.objects.create(
    #     title='SALE CREATE for tickes',
    #     data=str(data),
    #     message='Entro al bucle For para generar los tickets',
    #     nivel='I',
    #     user=client
    # )
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.ERROR_CORRECT_L,
        box_size=10,
        border=4
    )

    data = '{}'.format(ticket.code)

    qr.add_data(data)
    qr.make(fit=True)
    img = qr.make_image()
    buffer = BytesIO()
    img.save(buffer)
    filename = 'ticket-%s.png' % ticket.id
    filebuffer = InMemoryUploadedFile(
        buffer, None, filename, 'image/png', None, None)
    ticket.qr.save(filename, filebuffer)
    ticket.sales = sale_obj
    ticket.status = 'c'
    ticket.save()
    zone_ticket.is_bought = True
    zone_ticket.ticket = ticket
    zone_ticket.save()
    send_email_sale.apply_async((), {
        'sale_id': sale_obj.id,
        'owner_id': client.id
    })

def check_if_user_can_register_in_event(user: User, event: Event) -> bool:
    """
    Check if the user can register in the event.
    The user can only use at most event.max_invitation_uses_per_user invitation
    codes.
    """
    event_user_registrations = EventUserRegistration.objects.filter(
        user=user, event=event
    ).count()

    can_register = False
    if event_user_registrations < event.max_invitation_uses_per_user:
        can_register = True
    return can_register
