from rest_framework.permissions import BasePermission
from apps.accounts.models import User


class IsAdminOfEvent(BasePermission):
    def has_permission(self, request, view):
        return User.objects.filter(pk=request.user.pk,event_admin=view.kwargs.get("pk")).exists()