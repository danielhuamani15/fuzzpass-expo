from drf_extra_fields.geo_fields import PointField
from easy_thumbnails.templatetags.thumbnail import thumbnail_url
from rest_framework import serializers

from apps.payment.models import Ticket
from apps.web.models import Event, Zone, Producer


class ZoneInEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zone
        fields = ['id', 'name', 'value']


class ProducerInEventSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producer
        fields = ['id', 'name']


class ListEventSerializer(serializers.ModelSerializer):
    is_wish = serializers.SerializerMethodField()
    cheaper_zone = serializers.SerializerMethodField()
    image_sharpen = serializers.SerializerMethodField()
    zones = ZoneInEventSerializer(many=True, read_only=True)
    location = PointField(source='venue.location')
    producer_name = serializers.CharField(source='organizer.name', read_only=True)
    address = serializers.CharField(source='get_address')
    currency_symbol = serializers.CharField(read_only=True)

    def get_is_my_post(self, obj):
        band = False
        if self.context.get('request').user:
            user = self.context.get('request').user
            if user.is_authenticated():
                if obj.pet.user == user:
                    band = True
        return band

    # mal
    def get_is_wish(self, obj):
        band = False
        if self.context.get('request').user.is_authenticated():
            user_id = self.context.get('request').user.id
            for wish in obj.wishes.all():
                if wish.user.id == user_id:
                    band = wish.status
        return band

    def get_image_sharpen(self, obj):
        if obj.front:
            return self.context['request'].build_absolute_uri(thumbnail_url(obj.front, 'normal_full'))
        else:
            return None

    def get_cheaper_zone(self, obj):
        zone_values = []
        for zone in obj.zones.all():
            zone_values.append(zone.value)

        cheaper_zone = min(zone_values)
        return cheaper_zone

    class Meta:
        model = Event
        fields = ['id', 'name', 'front', 'image_sharpen', 'slug_name',
                  'start_date', 'end_date', 'presale_end_date', 'address',
                  'assistants', 'cheaper_zone', 'zones', 'is_wish', 'location',
                  'producer_name', 'currency_symbol', 'invitation']


class CodeSerializer(serializers.Serializer):
    code = serializers.CharField(required=True)


class TicketSerializer(serializers.Serializer):
    zone = serializers.SerializerMethodField()

    def get_zone(self, obj):
        zone = obj.event.zones.name
        return zone

    class Meta:
        model = Ticket
        fields = ['code', 'qr', 'email', 'dni', 'full_name', 'status', 'event', 'zone', 'price', 'sent_date']


class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
