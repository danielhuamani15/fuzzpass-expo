from rest_framework.pagination import PageNumberPagination


class TwoPostPagination(PageNumberPagination):
    page_size = 100


class FivePostPagination(PageNumberPagination):
    page_size = 100


class ThirtyEventPagination(PageNumberPagination):
    page_size = 100
