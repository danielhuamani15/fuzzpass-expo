from django.test import TestCase

from apps.accounts.models import User
from apps.venues.models import Venue
from apps.web.models import Event, EventUserRegistration, Producer, City
from apps.web.utils import check_if_user_can_register_in_event


class TestUtils(TestCase):
    def setUp(self):
        self.city = City.objects.create(name='test_city')
        self.venue = Venue.objects.create(
            name='test_venue_name',
            address='test_venue_address',
            city=self.city
        )
        self.producer = Producer.objects.create(name='test_producer')
        self.user = User.objects.create(
            email='test@example.com',
            password='test'
        )

        self.event = Event.objects.create(
            name='test_event',
            organizer=self.producer,
            venue=self.venue,
            max_invitation_uses_per_user=2
        )

    def test_if_user_can_register_in_event(self):
        self.assertTrue(
            check_if_user_can_register_in_event(self.user, self.event)
        )

    def test_if_user_can_register_in_event__with_one_free_registration(self):
        _ = EventUserRegistration.objects.create(
            user=self.user,
            event=self.event
        )

        self.assertTrue(
            check_if_user_can_register_in_event(self.user, self.event)
        )

    def test_check_if_user_can_register_in_event__full(self):
        _ = EventUserRegistration.objects.create(
            user=self.user,
            event=self.event
        )
        _ = EventUserRegistration.objects.create(
            user=self.user,
            event=self.event
        )
        self.assertFalse(
            check_if_user_can_register_in_event(self.user, self.event)
        )
