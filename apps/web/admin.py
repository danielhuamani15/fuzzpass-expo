from django.contrib import admin
from ..web.forms import LocationForm
from ..web.models import HomeBanner, EventUserRegistration, ZoneTicketFile, ZoneTicket, LogsEvent, Event, ImageEvent, Zone, Producer, Social, Spotify, Phone, City, Wish, VideoEvent
from django.db import models
from django import forms
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from import_export.fields import Field


class AdminImageEventInline(admin.TabularInline):
    model = ImageEvent
    extra = 1


class AdminVideoEventInline(admin.TabularInline):
    model = VideoEvent
    extra = 1


class AdminSocialsEventInline(admin.TabularInline):
    model = Social
    extra = 1


class AdminPhonesProducerInline(admin.TabularInline):
    model = Phone
    extra = 1


class AdminZonesEventInline(admin.TabularInline):
    model = Zone
    extra = 1


@admin.register(Producer)
class ProducerAdmin(admin.ModelAdmin):
    list_display = ('slug_name', 'name',)
    search_fields = ['name']
    raw_id_fields = ['user']

    fieldsets = (
        (None, {
            'fields': (
                ('name', 'logo'),
                ('address', 'cover'),
                ('facebook', 'twitter', 'instagram'),
                'status', 'user'
            )
        }),
    )

    inlines = [
        AdminPhonesProducerInline
    ]

    def get_queryset(self, request):
        qs = super(ProducerAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs

class EventResource(resources.ModelResource):
    fecha = Field()
    organizer_name = Field()

    class Meta:
        model = Event
        fields = ('fecha', 'name', 'organizer_name')

    def dehydrate_fecha(self, obj):
        return obj.start_date.strftime('%d/%m/%Y  %H:%M')

    def dehydrate_organizer_name(self, obj):
        return obj.organizer.name

class EventAdmin(ImportExportModelAdmin):
    list_display = ('name', 'city')
    search_fields = ['name']
    fieldsets = (
        (None, {
            'fields': (
                ('status_commission', 'commission'),
                ('include_igv', 'include_commission'),
                ('invitation', ),
                ('name', 'assistants', 'numbers_entries', 'currency'),
                'start_date', 'end_date', 'presale_end_date',
                ('front', 'ticket_cover', 'color'),
                'description',
                'band_description',
                ('organizer', 'spotify', 'video'),
                ('facebook', 'twitter', 'instagram'),
                'venue', 'terms', 'private', 'allow_choosing_seats'
            )
        }),
    )

    inlines = [
        AdminSocialsEventInline,
        AdminZonesEventInline,
        AdminVideoEventInline
    ]

    formfield_overrides = {
        models.TextField: {
            'widget': forms.Textarea(attrs={'class': 'ckeditor'})
        },
    }

    resource_class = EventResource

    class Media:
        js = ('https://cdn.ckeditor.com/4.5.10/basic/ckeditor.js',)


class SpotifyAdmin(admin.ModelAdmin):
    list_display = ('name',)


class LogsEventAdmin(admin.ModelAdmin):
    list_display = ('title', 'message', 'user', 'type_log', 'created')
    search_fields = ('title',)
    list_filters = ('type_log',)


class ZoneTicketAdmin(admin.ModelAdmin):
    list_display = ('code', 'zone', 'is_bought', 'bought_by')
    search_fields = ('code',)
    list_filters = ('zone',)

# class ZoneTicketFileAdmin(admin.ModelAdmin):
#     list_display = ('code', 'zone', 'is_bought', 'bought_by')
#     search_fields = ('code',)
#     list_filters = ('zone',)

admin.site.register(Event, EventAdmin)
admin.site.register(Spotify, SpotifyAdmin)
admin.site.register(City)
admin.site.register(Wish)
admin.site.register(Zone)
admin.site.register(LogsEvent, LogsEventAdmin)
admin.site.register(ZoneTicket, ZoneTicketAdmin)
admin.site.register(ZoneTicketFile)
admin.site.register(EventUserRegistration)
admin.site.register(HomeBanner)