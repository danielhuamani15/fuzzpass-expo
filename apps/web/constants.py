LEVELS = (
    ('D', 'DEBUG'),
    ('I', 'INFO'),
    ('W', 'WARNING'),
    ('E', 'ERROR'),
)

TYPE_LOG_EVENT = (
    ('PC', 'Pre Pago Culqui'),
    ('C', 'Pago Culqui')
)

TYPE_EVENT = (
    ('PC', 'Pre Pago Culqui'),
    ('G', 'Gratuito')
)

STATUS_COMMISSION = (
    ('PTE', 'Pendiente'),
    ('PG', 'Pagado')
)