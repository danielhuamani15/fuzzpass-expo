# coding: utf-8
from __future__ import unicode_literals

from datetime import datetime
from django.contrib.gis.db import models
from django.db.models import Sum
from apps.accounts.models import User
from django.template.defaultfilters import slugify
from easy_thumbnails.fields import ThumbnailerImageField
from phonenumber_field.modelfields import PhoneNumberField

from apps.payment.models import Ticket
from apps.web.managers import EventManager
from apps.web.widgets import ColorPickerWidget
from ..accounts.models import User
from fuzzpass.utils.mixins import UidMixin
from .constants import LEVELS, TYPE_LOG_EVENT, STATUS_COMMISSION
from django.db.models.signals import post_save
from django.dispatch import receiver


class Phone(models.Model):
    description = models.CharField(max_length=48, help_text="Ingrese la descripción del teléfono",
                                   verbose_name="Descripción del teléfono", null=True, blank=True)
    phone = PhoneNumberField(blank=True)
    producers = models.ForeignKey('Producer', related_name='phones', blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Teléfonos'

    def __str__(self):
        return self.description


class Social(models.Model):
    name = models.CharField(max_length=120, null=True, blank=True)
    link = models.URLField(null=True, blank=True)
    events = models.ForeignKey('Event', related_name='socials', blank=True, null=True)

    class Meta:
        verbose_name_plural = 'Redes Sociales'

    def __str__(self):
        return self.name


class Zone(models.Model):
    name = models.CharField(max_length=45, help_text="Ingrese el nombre de la zona", verbose_name="Nombre de la zona")
    value = models.DecimalField(null=True, blank=True, max_digits=8, decimal_places=2)
    order = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name="Prioridad")
    commission = models.DecimalField(verbose_name="Comisión", default=0, blank=True, max_digits=8, decimal_places=2)
    events = models.ForeignKey('Event', related_name='zones', blank=True, null=True)
    quantity = models.IntegerField(verbose_name='Cantidad', default=100)
    ticket_cover = models.ImageField(upload_to='Ticket/covers', help_text="Ingrese la portada para el ticket",
                                     verbose_name="Portada de ticket", blank=True, null=True)
    class Meta:
        verbose_name_plural = 'Zonas'
        ordering = ['order']

    def __str__(self):
        return self.name

    def get_total_commission(self, quantity):
        return self.commission * quantity

    def get_total_price(self, quantity):
        return self.value * quantity

    def get_total_amount(self, quantity):
        return self.get_total_commission(quantity) + \
               self.get_total_price(quantity)

    @property
    def total_sale(self):
        return Ticket.objects.filter(
            zone__id=self.id, status__in=['c', 'e', 'u']).count()

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'value': float(self.value),
            'commission': float(self.commission),
            'quantity': self.quantity - self.total_sale
        }


class Spotify(models.Model):
    name = models.CharField(max_length=50, help_text="Ingrese el nombre identificador",
                            verbose_name="Nombre", null=True, blank=True, default='default name')
    spotify_user = models.CharField(max_length=50, help_text="Ingrese el usuario de spotify",
                                    verbose_name="Usuario Spotify", null=True, blank=True)
    spotify_playlist = models.CharField(max_length=80, help_text="Ingrese el código de la playlist",
                                        verbose_name="Playlist Spotify", null=True, blank=True)
    order = models.PositiveSmallIntegerField(null=True, blank=True, verbose_name="Orden")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Spotify'


class City(models.Model):
    name = models.CharField(max_length=48)
    status = models.BooleanField(verbose_name="Estado", help_text="Estado", default=True)
    slug_name = models.SlugField(editable=False, max_length=120, null=True, blank=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.slug_name = slugify(self.name)
        return super(City, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'

    def __str__(self):
        return self.name


class Producer(models.Model):
    name = models.CharField(max_length=48)
    slug_name = models.SlugField(editable=False, max_length=120, null=True, blank=True)
    logo = models.ImageField(upload_to='Producer/logos/')
    cover = models.ImageField(upload_to='Producer/covers/', null=True, blank=True)
    email = models.EmailField(max_length=150, null=True, blank=True)
    address = models.CharField(max_length=120, help_text='Ejemplo: Av. Pardo 222 Miraflores',
                               blank=True)
    status = models.BooleanField(verbose_name="Estado", help_text="Estado", default=True)
    facebook = models.URLField(verbose_name="Facebook", help_text="Ingrese la url de facebook", null=True, blank=True)
    twitter = models.URLField(verbose_name="Twitter", help_text="Ingrese la url de twitter", null=True, blank=True)
    instagram = models.URLField(verbose_name="Instagram", help_text="Ingrese la url de instagram", null=True, blank=True)
    user = models.ForeignKey(User, related_name='producers', null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.slug_name = slugify(self.name)
        return super(Producer, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name = 'Productora'
        verbose_name_plural = 'Productoras'

    def __str__(self):
        return self.name


class ColorField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 10
        super(ColorField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        kwargs['widget'] = ColorPickerWidget
        return super(ColorField, self).formfield(**kwargs)


class Event(UidMixin, models.Model):
    CURRENCY_PEN = 'PEN'
    CURRENCY_USD = 'USD'

    CURRENCY_CHOICES = (
        (CURRENCY_PEN, CURRENCY_PEN),
        (CURRENCY_USD, CURRENCY_USD),
    )
    type_event = models.CharField(max_length=120, help_text="Ingrese el tipo de evento",
                            verbose_name="tipo de evento",
                            )
    name = models.CharField(max_length=120, help_text="Ingrese el nombre del evento", verbose_name="Nombre del evento",
                            blank=False, null=False)
    status_commission = models.CharField(max_length=120, default='PTE', choices=STATUS_COMMISSION, verbose_name="Estado de la comisión")
    commission = models.FloatField(default=0, verbose_name='Comisión (%)')
    include_igv = models.BooleanField(default=False, verbose_name="Incluye IGV")
    include_commission = models.BooleanField(default=False, verbose_name="Incluye Comisión")
    slug_name = models.SlugField(editable=False, max_length=120, null=True, blank=True)
    assistants = models.SmallIntegerField(default=0, null=True, blank=True, verbose_name="Asistentes",
                                          help_text="Número de asistentes al evento")
    description = models.TextField(max_length=40000, help_text="Ingrese la descripción del evento",
                                   verbose_name="Descripción del evento")
    band_description = models.TextField(max_length=40000, help_text="Ingrese la descripción de las bandas",
                                        verbose_name="Acerca de las bandas", blank=True, null=True)
    facebook = models.URLField(verbose_name="Facebook", help_text="Ingrese la url de facebook", null=True, blank=True)
    twitter = models.URLField(verbose_name="Twitter", help_text="Ingrese la url de twitter", null=True, blank=True)
    instagram = models.URLField(verbose_name="Instagram", help_text="Ingrese la url de instagram", null=True, blank=True)
    start_date = models.DateTimeField(default=datetime.now, verbose_name="Fecha de inicio")
    end_date = models.DateTimeField(verbose_name="Fecha de fin", null=True, blank=True)
    presale_end_date = models.DateTimeField(
        verbose_name="Fecha de fin de preventa",
        default=datetime.now
    )
    ticket_cover = models.ImageField(upload_to='Ticket/covers', help_text="Ingrese la portada para el ticket",
                                     verbose_name="Portada de ticket", blank=True, null=True)
    front = ThumbnailerImageField(upload_to='Event/fronts', verbose_name='Portada', blank=True, null=True)
    video = models.URLField(verbose_name="Video", help_text="Ingrese la url del video", null=True, blank=True)
    spotify = models.ForeignKey(Spotify, verbose_name="Spotify", related_name='events', null=True, blank=True)
    organizer = models.ForeignKey(Producer, related_name='events', verbose_name="Organizador")
    numbers_entries = models.SmallIntegerField(default=10, null=True, blank=True, verbose_name="Número de entradas")
    status = models.BooleanField(verbose_name="Estado", help_text="Estado", default=True)
    terms = models.TextField(null=True)
    cover_color = models.CharField(max_length=6, help_text='Ejemplo: 2c2c2c', blank=True,
                                   verbose_name="Color Fondo Hex.", default='000000')
    color = ColorField(blank=True, default='', verbose_name='Fondo Ticket')
    event_admin = models.ForeignKey(User, related_name="events", null=True)
    private = models.BooleanField(verbose_name='¿Evento privado?', default=False)
    venue = models.ForeignKey('venues.Venue')

    address = models.CharField(max_length=120,
                               help_text='Ejemplo: CC La Noche. Psj. Sanchez Carrión 222 Barranco',
                               blank=True, verbose_name="Dirección")
    city = models.ForeignKey(City, related_name='events',
                             verbose_name="Cities", null=True)
    location = models.PointField("longitude/latitude", geography=True,
                                 null=True, blank=True)
    map_image = models.ImageField(upload_to='Event/maps',
                                  help_text="Ingrese la imagen del mapa del evento",
                                  verbose_name="Imagen del mapa del evento",
                                  blank=True, null=True)
    currency = models.CharField(max_length=3, choices=CURRENCY_CHOICES,
                                default=CURRENCY_PEN)
    allow_choosing_seats = models.BooleanField(default=False)

    objects = EventManager()
    invitation = models.BooleanField("Invitación", default=False)
    max_invitation_uses_per_user = models.IntegerField(default=1)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.slug_name = slugify(self.name)
        return super(Event, self).save(force_insert, force_update, using, update_fields)

    def zones_dict(self):
        return [zone.to_dict() for zone in self.zones.all()]

    def get_address(self):
        if self.venue:
            return self.venue.full_address
        return ''

    def ticket_bought_count(self):
        return self.tickets.filter(is_invitation=False).exclude(status='f').count()

    @property
    def currency_symbol(self):
        return 'S/' if self.currency == self.CURRENCY_PEN else '$'

    class Meta:
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'

    def __str__(self):
        return self.name


class ImageEvent(models.Model):
    description = models.CharField(max_length=48, help_text="Ingrese la descripción de la imágen",
                                   verbose_name="Descripción de la imágen", null=True, blank=True)
    image = models.ImageField(upload_to='Events/covers')
    events = models.ForeignKey(Event, related_name='images')


class VideoEvent(models.Model):
    video = models.URLField(verbose_name="Video", help_text="Ingrese la url del video", null=True, blank=True)
    events = models.ForeignKey(Event, related_name='videos')

    class Meta:
        verbose_name = 'Video'
        verbose_name_plural = 'Videos'


class Wish(models.Model):
    status = models.BooleanField(verbose_name="Estado", help_text="Estado", default=False)
    events = models.ForeignKey(Event, related_name='wishes')
    user = models.ForeignKey(User, related_name='wishes')

    def __unicode__(self):
        return self.user.first_name


class LogsEvent(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    nivel = models.CharField('NIVEL', max_length=1, choices=LEVELS, default='D')
    data = models.TextField('Data', blank=True)
    title = models.CharField(max_length=120)
    message = models.TextField('Mensaje', blank=True)
    type_log = models.CharField(max_length=120, choices=TYPE_LOG_EVENT, default='PC')
    user = models.ForeignKey(User, related_name='user_logs_events', null=True)
    class Meta:
        verbose_name = 'Log'
        verbose_name_plural = 'Logs'

    def __unicode__(self):
        return self.mensaje


class ZoneTicket(models.Model):
    code = models.CharField('Code', max_length=120)
    zone = models.ForeignKey('Zone', related_name='zone_tickets_set')
    is_bought = models.BooleanField('es comprado', default=False)
    bought_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                                  related_name='bought_zone_tickets', blank=True)
    ticket = models.ForeignKey('payment.Ticket', on_delete=models.SET_NULL, null=True,
                                  related_name='ticket_zone_tickets', blank=True)
    class Meta:
        verbose_name = "ZoneTicket"
        verbose_name_plural = "ZoneTicket"

    def __str__(self):
        return self.code


class ZoneTicketFile(models.Model):
    file = models.FileField('file', upload_to='ticketfile')
    zone = models.ForeignKey('Zone', related_name='zone_ticket_file_set')

    class Meta:
        verbose_name = "ZoneTicketFile"
        verbose_name_plural = "ZoneTicketFiles"

    def __str__(self):
        return self.zone.events.name

@receiver(post_save, sender=ZoneTicketFile)
def zoneticketfile_save(sender, instance, created, **kwargs):
    if created:
        file = instance.file.path
        list_bulk = []
        with open(file, 'r') as f:
            for read in f.readlines():
                list_bulk.append(ZoneTicket(
                    code=read.strip(),
                    zone=instance.zone
                ))
        ZoneTicket.objects.bulk_create(list_bulk)


class EventUserRegistration(models.Model):
    user = models.ForeignKey(User)
    event = models.ForeignKey(Event)
    createad = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} - {}'.format(self.event.name, self.user.email)


class HomeBanner(models.Model):
    enlace = models.URLField(blank=True)
    banner = models.ImageField(upload_to='banner')
    position = models.IntegerField(default=1)
    class Meta:
        verbose_name = "HomeBanner"
        verbose_name_plural = "HomeBanners"
        ordering = ['position']

    def __str__(self):
        return str(self.position)
