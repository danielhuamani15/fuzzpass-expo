from django import template
from django.db.models import Sum, Q
from apps.payment.models import Ticket

register = template.Library()


def to_and(value):
    return str(value).replace(",", ".")

register.filter('to_and', to_and)


def is_in(var, args):
    if args is None:
        return False
    return var in args

register.filter(is_in)

def parse_int(value):
    return int(value)

register.filter(parse_int)

def ticket_filter(event, id_zone=None):
    tickets = Ticket.objects.exclude(status='f').filter(event=event, is_invitation=False)
    if id_zone:
        tickets = tickets.filter(zone_id=id_zone)
    price__sum = tickets.aggregate(Sum('price'))
    mount = 0
    if price__sum.get('price__sum'):
        mount = price__sum.get('price__sum')
    commission = event.commission
    total_commision = (float(mount)*float(commission))/100.00
    if event.include_igv:
        total_commision = total_commision + total_commision*0.18
    total_commision = round(total_commision, 2)
    total_cobrar = float(mount) - total_commision
    data = {
        'count': tickets.count(),
        'mount': mount,
        'total_commision': total_commision,
        'total_cobrar': total_cobrar

    }
    return data

register.filter(ticket_filter)