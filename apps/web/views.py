# coding: utf-8
from braces.views import AnonymousRequiredMixin
from class_based_auth_views.views import LoginView
from django.forms.models import model_to_dict
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.http.response import JsonResponse
from django.contrib.sites.shortcuts import get_current_site
from datetime import datetime
from apps.influences.models import InfluencesEvent
from apps.section.models import HowWorks, AboutUs, Terms, Politics
from django.shortcuts import render
from .permissions import IsAdminOfEvent
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import TemplateView, FormView
from django.core.urlresolvers import reverse_lazy
from django.views.generic import CreateView, ListView, DetailView
from django.contrib.auth import login
from django.views.generic.edit import ModelFormMixin
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.generics import ListAPIView, RetrieveAPIView, GenericAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.accounts.models import User
from apps.accounts.forms import CreateUserForm, AuthenticationForm
from apps.payment.models import Ticket
from apps.web.paginations import FivePostPagination, ThirtyEventPagination
from apps.web.serializers import ListEventSerializer, CodeSerializer, TicketSerializer
from apps.web.models import Event, Producer, City, Wish, ZoneTicket, EventUserRegistration, HomeBanner
from apps.web.forms import ValidateCodeForm
from apps.web.utils import generate_zone_ticket, check_if_user_can_register_in_event
from apps.web.templatetags.web_extras import ticket_filter
import xlsxwriter


class InformationView(LoginRequiredMixin, TemplateView):
    template_name = 'pages/information.html'


class SignupLoginView(AnonymousRequiredMixin, LoginView, CreateView):
    model = User
    template_name = 'pages/login_signup.html'
    form_class = CreateUserForm
    second_form_class = AuthenticationForm
    success_url = reverse_lazy('accounts:profile')

    def get_context_data(self, **kwargs):
        context = super(SignupLoginView, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class(request=self.request)
        if 'form2' not in context:
            context['form2'] = self.second_form_class(request=self.request)
        return context

    def get_success_url(self):
        return reverse_lazy('accounts:profile')

    def post(self, request, *args, **kwargs):
        if 'form' in request.POST:
            form_class = self.get_form_class()
            form_name = 'form'
        else:
            form_class = self.second_form_class
            form_name = 'form2'

        form = self.get_form(form_class)

        if form_name == 'form':
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.second_form_invalid(**{form_name: form})

        elif form_name == 'form2':
            if form.is_valid():
                return self.second_form_valid(form)
            else:
                return self.second_form_invalid(**{form_name: form})

    def form_valid(self, form):
        self.object = form.save()
        self.object.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, self.object)
        return super(ModelFormMixin, self).form_valid(form)

    def second_form_valid(self, form):
        """
        The user has provided valid credentials (this was checked in AuthenticationForm.is_valid()). So now we
        can check the test cookie stuff and log him in.
        """
        self.check_and_delete_test_cookie()
        login(self.request, form.get_user())
        return HttpResponseRedirect(self.get_success_url())

    def second_form_invalid(self, form):
        """
        The user has provided invalid credentials (this was checked in AuthenticationForm.is_valid()). So now we
        set the test cookie again and re-render the form with errors.
        """
        self.set_test_cookie()
        return super(LoginView, self).form_invalid(form)


class HomeView(ListView):
    template_name = 'pages/home.html'
    context_object_name = 'events'
    model = Event
    paginate_by = 12

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['list_cities'] = City.objects.filter(status=True).order_by('name')
        return context

    def get_queryset(self):
        subdomain = self.request.subdomain
        return Event.objects.public(subdomain).order_by('start_date')

    def get(self, *args, **kwargs):
        return redirect('/eventos/')

class EventsView(ListView):
    template_name = 'pages/pages2.html'
    context_object_name = 'events'
    model = Event

    def get_context_data(self, **kwargs):
        context = super(EventsView, self).get_context_data(**kwargs)
        context['events'] = Event.objects.all().order_by('name')
        context['home_banners'] = HomeBanner.objects.all()
        return context

    # def get_queryset(self):
    #     subdomain = self.request.subdomain
    #     if self.kwargs.get('event_city') and self.kwargs.get('event_month'):
    #         return Event.objects.public(subdomain).filter(
    #             city_id=self.kwargs.get('event_city')).filter(
    #             start_date__month=self.kwargs.get('event_month')).order_by(
    #             'start_date')
    #     elif self.kwargs.get('event_city'):
    #         return Event.objects.public(subdomain).filter(
    #             city_id=self.kwargs.get('event_city')).order_by(
    #             'start_date')
    #     elif self.kwargs.get('event_month'):
    #         return Event.objects.public(subdomain).filter(
    #             start_date__month=self.kwargs.get('event_month')).order_by(
    #             'start_date')
    #     return Event.objects.public(subdomain).order_by('start_date')


class EventDetail(FormView, DetailView):
    template_name = 'pages/event.html'
    context_object_name = 'event'
    model = Event
    subdomain = None
    form_class = ValidateCodeForm

    def get_success_url(self):
        return reverse_lazy('webs:my_zone_tickets', kwargs={
                'slug_name': self.get_object().slug_name
            })

    def get_context_data(self, **kwargs):
        context = super(EventDetail, self).get_context_data(**kwargs)
        context['events'] = Event.objects.public(self.subdomain).order_by('start_date')
        context['list_cities'] = City.objects.filter(status=True).order_by('name')
        event = get_object_or_404(Event, slug_name__iexact=self.kwargs.get('slug_name'))
        context['numbers_entries'] = range(1, event.numbers_entries + 1)
        context['inside_eventInfluence'] = False
        context['currentDate'] = datetime.now()
        if InfluencesEvent.objects.filter(influence__id=self.request.user.pk).filter(event__id=event.id):
            influence_assigned = True
        else:
            influence_assigned = False

        if InfluencesEvent.objects.filter(event__id=event.pk):
            is_event_assigned = True
        else:
            is_event_assigned = False

        if event.invitation and self.request.user.is_authenticated():
            user_can_register = check_if_user_can_register_in_event(
                self.request.user,
                event
            )
            context['user_can_register'] = user_can_register

        context['is_influence_assigned'] = influence_assigned
        context['is_event_assigned'] = is_event_assigned
        context['event_session'] = self.request.session.get('event')
        return context

    def get_object(self, queryset=None):
        self.subdomain = self.request.subdomain
        return get_object_or_404(
            Event.objects.all(self.subdomain),
            slug_name__iexact=self.kwargs.get('slug_name')
        )

    def post(self, request, *args, **kwargs):
        """
        Handle POST requests: instantiate a form instance with the passed
        POST variables and then check if it's valid.
        """
        self.object = self.get_object()
        event = self.object
        user = self.request.user

        code = request.POST.get('code_validate')
        zone_event = ZoneTicket.objects.filter(
            code=code,
            zone__events__id=event.id
        )
        form = self.get_form()
        if not zone_event.exists():
            form.add_error('code_validate', 'Código ingresado es invalido')
        if form.is_valid():
            request.session['event'] = self.object.slug_name
            _ = EventUserRegistration.objects.create(
                user=user,
                event=event
            )
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class EventzDetail(LoginRequiredMixin, DetailView):
    template_name = 'pages/event.html'
    context_object_name = 'event'
    model = Event
    subdomain = None

    def get_context_data(self, **kwargs):
        context = super(EventzDetail, self).get_context_data(**kwargs)
        context['events'] = Event.objects.public(self.subdomain).order_by('start_date')
        context['list_cities'] = City.objects.filter(status=True).order_by('name')
        event = get_object_or_404(Event, slug_name__iexact=self.kwargs.get('slug_name'))
        context['numbers_entries'] = range(1, event.numbers_entries + 1)
        return context

    def get_object(self, queryset=None):
        self.subdomain = self.request.subdomain
        return get_object_or_404(Event.objects.all(self.subdomain), slug_name__iexact=self.kwargs.get('slug_name'))


class ProducersView(ListView):
    template_name = 'pages/producers.html'
    context_object_name = 'producers'
    model = Producer


class ProducerDetail(DetailView):
    template_name = 'pages/producer.html'
    context_object_name = 'producer'


    def get_object(self, queryset=None):
        return get_object_or_404(Producer.objects.filter(status=True), slug_name__iexact=self.kwargs.get('slug_name'))

    def get_context_data(self, **kwargs):
        subdomain = self.request.subdomain
        context = super(ProducerDetail, self).get_context_data(**kwargs)
        context['list_events'] = Event.objects.public(subdomain).order_by('start_date')
        return context


class hwView(ListView):
    template_name = 'pages/howItWork.html'
    context_object_name = 'events'
    model = Event
    paginate_by = 12
    subdomain = None

    def get_queryset(self):
        self.subdomain = self.request.subdomain
        return Event.objects.public(self.subdomain).order_by('start_date')

    def get_context_data(self, **kwargs):
        context = super(hwView, self).get_context_data(**kwargs)
        context['list_events'] = Event.objects.public(self.subdomain).order_by('start_date')
        hws = HowWorks.objects.all().last()
        context['wh'] = hws
        return context


class abutsView(TemplateView):
    template_name = 'pages/aboutUs.html'

    def get_context_data(self, **kwargs):
        context = super(abutsView, self).get_context_data(**kwargs)
        about_info = AboutUs.objects.filter().last()
        context['about_info'] = about_info
        return context


class ticketsView(TemplateView):
    template_name = 'reuse/tickets.html'


class ListRandomEventsAPI(ListAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (AllowAny,)
    serializer_class = ListEventSerializer
    pagination_class = FivePostPagination

    def get_queryset(self):
        subdomain = self.request.subdomain
        qs = Event.objects.current_public(subdomain).order_by('start_date')
        qs = qs.select_related(
            'venue__city', 'organizer'
        ).prefetch_related('wishes__user', 'zones')
        return qs


class ListEventsProducerAPI(ListAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (AllowAny,)
    serializer_class = ListEventSerializer
    pagination_class = FivePostPagination

    def get_queryset(self):
        subdomain = self.request.subdomain
        return Event.objects.public(subdomain).filter(
            organizer__pk=self.kwargs.get("pk"))


class ListFilterEventsAPI(ListAPIView):
    authentication_classes = (SessionAuthentication,)
    permission_classes = (AllowAny,)
    serializer_class = ListEventSerializer
    pagination_class = FivePostPagination

    def get_queryset(self):
        subdomain = self.request.subdomain
        if self.kwargs.get('event_city') and self.kwargs.get('event_month'):
            return Event.objects.public(subdomain).filter(city_id=self.kwargs.get('event_city')).filter(
                start_date__month=self.kwargs.get('event_month')).order_by(
                'start_date')
        elif self.kwargs.get('event_city'):
            return Event.objects.public(subdomain).filter(city_id=self.kwargs.get('event_city')).order_by(
                'start_date')
        elif self.kwargs.get('event_month'):
            return Event.objects.public(subdomain).filter(start_date__month=self.kwargs.get('event_month')).order_by(
                'start_date')
        return Event.objects.public(subdomain).order_by('start_date')


class WishEventAPI(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        event = get_object_or_404(Event, id=self.kwargs.get('pk'))
        for wish in event.wishes.all():
            if wish.user.id == self.request.user.id and wish.status is False:
                wish.status = True
                wish.save()
                return Response({'detail': 'update wish'}, status=status.HTTP_200_OK)
        wish = Wish(events=event, user=self.request.user, status=True)
        wish.save()
        return Response({'detail': 'Wish'}, status=status.HTTP_200_OK)


class UnWishEventAPI(APIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        event = get_object_or_404(Event, id=self.kwargs.get('pk'))

        for wish in event.wishes.all():
            if wish.user.id == self.request.user.id:
                wish.status = False
                wish.save()
        return Response({'detail': 'Un Wish'}, status=status.HTTP_200_OK)


class EventAPIView(RetrieveAPIView):
    permission_classes = IsAuthenticated,
    serializer_class = ListEventSerializer

    def get_object(self):
        return self.request.user.event_admin


class TicketInEventAPIView(RetrieveAPIView):
    permission_classes = IsAuthenticated, IsAdminOfEvent
    serializer_class = TicketSerializer

    def get_object(self):
        return get_object_or_404(Ticket, event__pk=self.kwargs.get("pk"), code=self.kwargs.get("code"))


class UsedTicketsAPIView(ListAPIView):
    serializer_class = TicketSerializer
    pagination_class = ThirtyEventPagination

    def get_queryset(self):
        return Ticket.objects.filter(event__pk=self.kwargs.get("pk"), status="u")


class TicketValidateAPIView(GenericAPIView):
    permission_classes = IsAuthenticated, IsAdminOfEvent
    serializer_class = CodeSerializer

    def post(self, request, *args, **kwargs):
        code = self.request.data.get('code')
        ticket = get_object_or_404(Ticket, event__pk=self.kwargs.get("pk"), code=code)
        if ticket.status == 'c' or ticket.status == 'e':
            ticket.status = 'u'
            ticket.save()
            return Response(model_to_dict(ticket, fields=[], exclude=["qr"]))
        else:
            response = JsonResponse({'status': 'false', 'message': "Ticket no comprado"})
            response.status_code = 410
            return response


class ViewTerms(TemplateView):
    template_name = 'pages/terms.html'

    def get_context_data(self, **kwargs):
        context = super(ViewTerms, self).get_context_data(**kwargs)
        term = Terms.objects.all().last()
        context['term'] = term
        return context


class ViewAboutUs(TemplateView):
    template_name = 'pages/aboutUs.html'

    def get_context_data(self, **kwargs):
        context = super(ViewAboutUs, self).get_context_data(**kwargs)
        about_info = AboutUs.objects.all().last()
        context['about_info'] = about_info
        return context


class ViewPolitics(TemplateView):
    template_name = 'pages/politicsPrivate.html'

    def get_context_data(self, **kwargs):
        context = super(ViewPolitics, self).get_context_data(**kwargs)
        politic = Politics.objects.all().last()
        context['politic'] = politic
        return context


class UserConfirm(TemplateView):
    template_name = 'pages/userConfirm.html'


def validate_code(request):
    data = {
        'status': 'error',
        'msg': ''
    }
    if request.is_ajax():
        if request.user.is_authenticated():
            code = request.POST.get('code', '')
            id = request.POST.get('id', '')
            zone_event = ZoneTicket.objects.filter(
                code=code.strip(), zone__events__id=int(id), is_bought=False)
            if zone_event.exists():
                data['status'] = 'ok'
                # domain = get_current_site(request).domain
                generate_zone_ticket(zone_event, request.user)
                return JsonResponse(data)

            else:
                data['msg'] = 'El codigo ingresado no es valido'
                return JsonResponse(data)
        else:
            data['msg'] = 'Debe iniciar sesión'
            return JsonResponse(data)
    return JsonResponse(data)


def my_zone_tickets(request, slug_name):
    tickets = Ticket.objects.filter(owner=request.user).exclude(status='f')
    domain = get_current_site(request).domain
    ctx = {
        'tickets': tickets,
        'domain': domain
    }
    return render(request, 'pages/my-buy_tickets_zone.html', ctx)


class DashboardProducer(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name = 'pages/dashboard_productoras.html'

    def test_func(self):
        return self.request.user.producers.all().exists() or self.request.user.is_superuser

    def get_context_data(self, **kwargs):
        context = super(DashboardProducer, self).get_context_data(**kwargs)
        user = self.request.user
        if self.request.user.is_superuser:
            events_progress = Event.objects.filter(end_date__gte=datetime.now()).prefetch_related('tickets')
            events_past = Event.objects.filter(end_date__lte=datetime.now()).prefetch_related('tickets')
        else:
            events_progress = Event.objects.filter(organizer__user=user, end_date__gte=datetime.now()).prefetch_related('tickets')
            events_past = Event.objects.filter(organizer__user=user, end_date__lte=datetime.now()).prefetch_related('tickets')
        context['events_progress'] = events_progress
        context['events_past'] = events_past
        return context


class DashboardProducerDetail(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    template_name = 'pages/dashboard_productoras_detalle.html'
    model = Event
    slug_url_kwarg = 'slug_name'
    slug_field = 'slug_name'

    def test_func(self):
        return self.request.user.producers.all().exists() or self.request.user.is_superuser

    def get_queryset(self):
        queryset = super(DashboardProducerDetail, self).get_queryset()
        if not self.request.user.is_superuser:
            queryset = queryset.filter(organizer__user=self.request.user)
        return queryset.prefetch_related('tickets')
    # def get_object(self, queryset=None):
    #     return get_object_or_404(Producer.objects.filter(status=True), slug_name__iexact=self.kwargs.get('slug_name'))

    def get_context_data(self, **kwargs):
        context = super(DashboardProducerDetail, self).get_context_data(**kwargs)
        context['zones'] = self.get_object().zones.all()
        # context['tickets'] = Ticket.objects.exclude(status='f').filter(event=self.get_object())
        return context

def dashboard_producer_excel(request, slug_name):
    event = get_object_or_404(Event, slug_name=slug_name)
    response = HttpResponse(content_type='application/ms-excel')
    file_name = 'dashboard'
    today = datetime.now().strftime("%I:%M %p %d/%m/%Y ")
    response['Content-Disposition'] = 'attachment; filename="{0}_{1}.xlsx"'.format(file_name, today)
    workbook = xlsxwriter.Workbook(response, {'in_memory': True})
    worksheet = workbook.add_worksheet()
    worksheet.set_column('B:I', 21)
    row = 2
    col = 2
    worksheet.write(row, 2, 'Evento')
    worksheet.write(row, 4, 'Fecha')
    worksheet.write(row, 5, 'Productor')
    worksheet.write(row, 7, 'Estado')
    worksheet.write(row+1, 2, '{0}'.format(event.name))
    worksheet.write(row+1, 4, event.start_date.strftime("%I:%M %p %d/%m/%Y "))
    worksheet.write(row+1, 5, event.organizer.name)
    worksheet.write(row+1, 7, event.get_status_commission_display())
    worksheet.write(row+4, 3, 'TICKETS VENDIDOS')
    worksheet.write(row+4, 4, 'RECAUDACIÓN')
    worksheet.write(row+4, 5, 'COMISIÓN')
    worksheet.write(row+4, 6, 'COMISIÓN EN S/')
    worksheet.write(row+4, 7, 'TOTAL POR COBRAR')
    ticket = ticket_filter(event)
    commision = str(event.commission)
    if event.include_igv:
        commision = commision + '+ IGV'
    worksheet.write(row+5, 2, 'TODAS')
    worksheet.write(row+5, 3, ticket.get('count'))
    worksheet.write(row+5, 4, ticket.get('mount'))
    worksheet.write(row+5, 5, commision)
    worksheet.write(row+5, 6, ticket.get('total_commision'))
    worksheet.write(row+5, 7, ticket.get('total_cobrar'))
    row = row + 4
    for zone in event.zones.all():

        ticket = ticket_filter(event, zone.id)
        worksheet.write(row, 3, 'TICKETS VENDIDOS')
        worksheet.write(row, 4, 'RECAUDACIÓN')
        worksheet.write(row, 5, 'COMISIÓN')
        worksheet.write(row, 6, 'COMISIÓN EN S/')
        worksheet.write(row, 7, 'TOTAL POR COBRAR')

        worksheet.write(row+1, 2, zone.name)
        worksheet.write(row+1, 3, ticket.get('count'))
        worksheet.write(row+1, 4, ticket.get('mount'))
        worksheet.write(row+1, 5, commision)
        worksheet.write(row+1, 6, ticket.get('total_commision'))
        worksheet.write(row+1, 7, ticket.get('total_cobrar'))
        row = row + 3
    workbook.close()
    return response

# def dashboard_producer_user_excel(request, slug_name):
#     event = get_object_or_404(Event, slug_name=slug_name)
#     response = HttpResponse(content_type='application/ms-excel')
#     file_name = 'dashboard'
#     today = datetime.now().strftime("%I:%M %p %d/%m/%Y ")
#     response['Content-Disposition'] = 'attachment; filename="{0}_{1}.xlsx"'.format(file_name, today)
#     workbook = xlsxwriter.Workbook(response, {'in_memory': True})
#     worksheet = workbook.add_worksheet()
#     worksheet.set_column('B:I', 21)
#     row = 2
#     col = 2
#     workbook.close()
#     return response
# 406
# 1810100014