from django.contrib import admin

from apps.companies.models import Company
from apps.venues.admin import VenueInLine


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'subdomain', 'active')
    search_fields = ('name', )
    inlines = [VenueInLine, ]

