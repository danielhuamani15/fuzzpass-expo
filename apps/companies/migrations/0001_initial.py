# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-07-06 18:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('subdomain', models.CharField(max_length=20)),
                ('logo', models.ImageField(upload_to='')),
                ('logo_reverse', models.ImageField(upload_to='')),
                ('footer_color', models.CharField(max_length=10)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name': 'company',
                'verbose_name_plural': 'companies',
                'db_table': 'company',
            },
        ),
    ]
