from django.db import models
from django.utils.translation import ugettext_lazy as _


class Company(models.Model):
    name = models.CharField(max_length=250)
    subdomain = models.CharField(max_length=20)
    logo = models.ImageField(upload_to='companies/logos')
    logo_reverse = models.ImageField(upload_to='companies/logos')
    footer_color = models.CharField(max_length=10)
    whitelabel = models.BooleanField(default=False)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'company'
        verbose_name = _('company')
        verbose_name_plural = _('companies')
