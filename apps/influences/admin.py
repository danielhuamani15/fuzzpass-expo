from django.contrib import admin

from .models import InfluencesEvent


class InfluencesEventAdmin(admin.ModelAdmin):
    list_display = ('influence', 'event', 'unique_link',)
    exclude = ('unique_link',)

    def Influencer(self, obj):
        return obj.influence


admin.site.register(InfluencesEvent, InfluencesEventAdmin)
