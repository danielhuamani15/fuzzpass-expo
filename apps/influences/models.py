import hashlib
from ..accounts.models import User
from ..web.models import Event
from django.db import models


class InfluencesEvent(models.Model):
    influence = models.ForeignKey(User, related_name='influences', verbose_name="Influencer")
    event = models.ForeignKey(Event, related_name='events')
    unique_link = models.CharField(max_length=250, help_text="Link unico", verbose_name="Link Influencer", blank=True, null=True)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.unique_link = "https://fuzzpass.com/f_i/" + self.influence.uid + '/' + self.event.uid
        return super(InfluencesEvent, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        verbose_name_plural = 'Influencers'
        verbose_name = 'Influencer'
