from django.apps import AppConfig


class InfluencesConfig(AppConfig):
    name = 'influences'
