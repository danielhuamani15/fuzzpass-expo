# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-10-13 20:06
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('influences', '0004_auto_20161013_1034'),
    ]

    operations = [
        migrations.AlterField(
            model_name='influencesevent',
            name='event',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='events', to='web.Event'),
        ),
        migrations.AlterField(
            model_name='influencesevent',
            name='influence',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='influences', to=settings.AUTH_USER_MODEL),
        ),
    ]
