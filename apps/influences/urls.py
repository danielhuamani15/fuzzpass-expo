from django.conf.urls import url
from .views import InfluenceEventDetail

urlpatterns = [
    url(r'^f_i/(?P<influence>[-\w]+)/(?P<event>[-\w]+)/$', InfluenceEventDetail.as_view(), name="inf-event_detail")
]
