from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, get_object_or_404
from django.views.generic import DetailView
from datetime import datetime
from ..accounts.models import User
from ..web.models import Event, City


# class InfluenceEventDetail(LoginRequiredMixin,DetailView):
class InfluenceEventDetail(DetailView):
    template_name = 'pages/event.html'
    context_object_name = 'event'
    model = Event

    def get_context_data(self, **kwargs):
        context = super(InfluenceEventDetail, self).get_context_data(**kwargs)
        influence = self.kwargs.get('influence')
        influence_event = self.kwargs.get('event')

        if not influence == '':
            context['is_URIInfluence'] = True

        influence_pk = settings.HASHIDS.decode(influence)[0]
        event_pk = settings.HASHIDS.decode(influence_event)[0]
        context['uid_inf'] = influence
        context['inside_eventInfluence'] = True
        context['events'] = Event.objects.filter(status=True).order_by('start_date')
        context['list_cities'] = City.objects.filter(status=True).order_by('name')
        event = get_object_or_404(Event, id=event_pk)
        context['influence'] = get_object_or_404(User, id=influence_pk)
        context['numbers_entries'] = range(1, event.numbers_entries + 1)
        context['currentDate'] = datetime.now()

        if self.request.user.pk == influence_pk:
            context['is_influence_assigned'] = True
        else:
            context['is_influence_assigned'] = False

        # if InfluencesEvent.objects.filter(influence__id=self.request.user.pk) and InfluencesEvent.objects.filter(event__id=event.pk):
        #     context['is_influence_assigned'] = True
        # else:
        #     context['is_influence_assigned'] = False

        return context

    def get_object(self, queryset=None):
        return get_object_or_404(Event.objects.filter(status=True).filter(id=settings.HASHIDS.decode(self.kwargs.get('event'))[0]))
