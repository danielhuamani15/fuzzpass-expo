from django.contrib import admin
from django.contrib.admin.filters import SimpleListFilter

from apps.web.models import Zone
from apps.payment.models import Ticket, Sale, CulquiCustomer, CulquiCard
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from import_export.fields import Field


class TicketResource(resources.ModelResource):
    fecha = Field()
    # status_invitacion = Field()

    class Meta:
        model = Ticket
        # fields = ('id', 'fecha', 'code', 'email', 'dni', 'full_name', 'status',
        #           'event__name', 'owner__first_name', 'owner__last_name',
        #           'owner__email', 'zone__name', 'price', 'sent_date', 'seat',
        #           'seat_section', 'status_invitacion')
        fields = ('id', 'fecha', 'code', 'status',
                  'event__name', 'owner__first_name', 'owner__last_name',
                  'owner__email', 'zone__name', )
        # export_order = ('id', 'fecha', 'code', 'email', 'dni', 'full_name', 'status',
        #           'event__name', 'owner__first_name', 'owner__last_name',
        #           'owner__email', 'zone__name', 'price', 'sent_date', 'seat',
        #           'seat_section', 'status_invitacion')
        export_order = ('id', 'fecha', 'code', 'status',
                  'event__name', 'owner__first_name', 'owner__last_name',
                  'owner__email', 'zone__name', )

    def dehydrate_status_invitacion(self, obj):
        if obj.is_invitation:
            return 'Si'
        return 'No'

    def dehydrate_fecha(self, obj):
        if obj.sales:
            return obj.sales.date_created.strftime('%d/%m/%Y  %H:%M')
        return ''

class SaleAdmin(admin.ModelAdmin):
    model = Sale

    list_display = ('fecha_compra', 'codigo_venta', 'nombre_y_apellidos',
                    'correo', 'tickets', 'source_type', 'source_person')
    list_select_related = ('client',)

    # def codigo_del_evento(self, obj):
    #     first_ticket = obj.tickets_sales.first()
    #     return first_ticket.event.name

    def codigo_venta(self, obj):
        return obj.uid

    def fecha_compra(self, obj):
        return obj.date_created

    def tickets(self, obj):
        return obj.tickets_sales.count()

    def correo(self, obj):
        return obj.client.email

    def nombre_y_apellidos(self, obj):
        return obj.client.first_name + ' ' + obj.client.last_name

    def get_queryset(self, request):
        queryset = super(SaleAdmin, self).get_queryset(request)
        return queryset.prefetch_related('tickets_sales')


class ReportTicketAdmin(admin.ModelAdmin):
    model = Ticket

    list_display = (
        'codigo_venta', 'fecha_de_compra', 'codigo_del_ticket',
        'qr_del_ticket', 'nombre_comprador', 'dni_comprador',
        'mail_comprador', 'sexo_comprador', 'price_ticket_sin_comision',
        'comision', 'price_venta_de_ticket', 'ciudad_de_compra',
        'codigo_del_evento', 'codigo_del_productor', 'codigo_del_promotor',
        'nombre_del_asistente', 'dni_del_asistente', 'mail_del_asistente',
        'sexo_del_asistente'
    )
    list_select_related = ('sales', 'owner', 'event__organizer')

    def codigo_venta(self, obj):
        return obj.sales.uid

    def fecha_de_compra(self, obj):
        return obj.sales.date_created

    def codigo_del_ticket(self, obj):
        return obj.code

    def nombre_comprador(self, obj):
        return obj.owner.first_name + ' ' + obj.owner.last_name

    def dni_comprador(self, obj):
        return obj.owner.dni

    def mail_comprador(self, obj):
        return obj.owner.email

    def sexo_comprador(self, obj):
        return obj.owner.get_gender_display()

    def price_ticket_sin_comision(self, obj):
        return obj.price

    def comision(self, obj):
        return '10%'

    def price_venta_de_ticket(self, obj):
        obj.price -= (obj.price / 100)
        return obj.price

    def ciudad_de_compra(self, obj):
        return 'Lima'

    def canal_de_compra(self, obj):
        return 'Culqi'

    def codigo_del_evento(self, obj):
        return obj.event.uid

    def codigo_del_productor(self, obj):
        return obj.event.organizer.name

    def codigo_del_promotor(self, obj):
        return obj.sales.source_person

    def nombre_del_asistente(self, obj):
        return obj.full_name

    def dni_del_asistente(self, obj):
        return '12345678'

    def mail_del_asistente(self, obj):
        return obj.email

    def sexo_del_asistente(self, obj):
        return 'Masculino'

    def qr_del_ticket(self, obj):
        return u'<img  width="{}" height={} src="{}" />'.format(64, 64, obj.qr.url)

    qr_del_ticket.short_description = 'Qr Ticket'
    qr_del_ticket.allow_tags = True


class EventFilter(SimpleListFilter):
    title = 'event'
    parameter_name = 'event'

    # def lookups(self, request, model_admin):
    #     tickets = Ticket.objects.all().select_related(
    #         'event'
    #     ).order_by('event__name')
    #     if not request.user.is_superuser:
    #         tickets = tickets.filter(event__organizer__user=request.user)
    #     events = set([t.event for t in tickets])
    #     return [(e.id, e.name) for e in events]
    def lookups(self, request, model_admin):
        tickets = Ticket.objects.all().select_related(
            'event'
        ).order_by('event__name')
        if not request.user.is_superuser:
            tickets = tickets.filter(event__organizer__user=request.user)
        tickets = Ticket.objects.filter(id__in=tickets.values_list('id', flat=True)).distinct('event')
        return tickets.values_list('event_id', 'event__name')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(event__id__exact=self.value())
        else:
            return queryset


class ZoneFilter(SimpleListFilter):
    title = 'zone'
    parameter_name = 'zone'

    def lookups(self, request, model_admin):
        if 'event' in request.GET:
            event = request.GET['event']
            zones = Zone.objects.filter(events__id__exact=event)
        else:
            zones = Zone.objects.all()
        return zones.values_list('id', 'name')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(zone__id__exact=self.value())
        else:
            return queryset


class StatusEventFilter(SimpleListFilter):
    title = 'status'
    parameter_name = 'status'

    def lookups(self, request, model_admin):
        return [(0, 'borrador'), (1, 'comprado')]

    def queryset(self, request, queryset):
        if self.value() == '0':
            return queryset.filter(status='f')
        elif self.value() == '1':
            return queryset.exclude(status='f')
        else:
            return queryset


class TicketAdmin(ImportExportModelAdmin):
    model = Ticket
    list_filter = (StatusEventFilter, ZoneFilter, EventFilter)
    list_display = ('codigo_venta', 'sale_date', 'nombre_evento', 'price',
                    'full_name_remitente', 'email_remitente', 'zone', 'is_invitation', 'status',
                    'full_name', 'email', 'sexo_r', 'sent_date', 'seat',)
    search_fields = ['code', 'seat']
    list_select_related = ('event', 'owner', 'sales', 'zone')
    list_editable = ('status', 'is_invitation')

    def get_queryset(self, request):
        return super(TicketAdmin, self).get_queryset(request).select_related('web')

    def fecha_evento(self, obj):
        return obj.event.start_date

    def nombre_evento(self, obj):
        return obj.event.name

    def sexo_r(self, obj):
        return obj.owner.gender

    def codigo_venta(self, obj):
        if obj.sales:
            return obj.sales.uid
        return ''

    def full_name_remitente(self, obj):
        return obj.owner.get_full_name()

    def email_remitente(self, obj):
        return obj.owner.email

    def sale_date(self, obj):
        if obj.sales:
            return obj.sales.date_created
        return ''

    fieldsets = (
        (None, {
            'fields': (
                ('code', 'qr', 'price'),
                ('status', 'zone', 'is_invitation'),
                ('seat', 'seat_section', 'owner'),
                ('event',)
            )
        }),
    )

    resource_class = TicketResource

    def get_queryset(self, request):
        qs = super(TicketAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(
            event__organizer__user=request.user
        ).exclude(
            status='f'
        )

    def get_readonly_fields(self, request, obj=None):
        if request.user.is_superuser:
            return self.readonly_fields
        return self.fields or [f.name for f in self.model._meta.fields]

    def has_add_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

    def has_change_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return (request.method in ['GET', 'HEAD'] and
                super().has_change_permission(request, obj))

    def get_list_display(self, request):
        if request.user.is_superuser:
            return self.list_display
        return ('codigo_venta', 'sale_date', 'nombre_evento',
                'full_name_remitente', 'zone', 'status', 'full_name',
                'sent_date')


class Report(Ticket):
    class Meta:
        proxy = True
        verbose_name_plural = "Reporte Principal"


class MyPostAdmin(TicketAdmin):
    def get_queryset(self, request):
        return self.model.objects.filter(user=request.user)


admin.site.register(Ticket, TicketAdmin)
admin.site.register(Sale, SaleAdmin)
admin.site.register(Report, ReportTicketAdmin)
admin.site.register(CulquiCard)
admin.site.register(CulquiCustomer)