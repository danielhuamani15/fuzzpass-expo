from django.conf.urls import url

from .views import BuyDetail, SaleCreate, ValidateSaleView, ValidateSaleCardView, ListMyTicketsView,\
    SaleZeroCreate, SendTicketAPIView, ReSentAPIView, BuyInfluenceDetail, \
    ChooseSeats, BuyDetailRegisterCard, send_ticket

urlpatterns = [
    url(r'^buy-in/(?P<slug_name>[-\w]+)/$', BuyDetail.as_view(), name="buy_in"),
url(r'^buy-in/(?P<slug_name>[-\w]+)/test/$', BuyDetailRegisterCard.as_view(), name="buy_in_register_card"),
    url(r'^buy-from_influence/(?P<uid_inf>[-\w]+)/(?P<uid_evn>[-\w]+)/$', BuyInfluenceDetail.as_view(), name="buy_from-influence"),
    url(r'^choose-seats/(?P<slug_name>[-\w]+)/(?P<zone_id>[-\w]+)/$',
        ChooseSeats.as_view(), name='choose_seats'),
    url(r'^create/sale/$', SaleCreate.as_view(), name="create_sale"),
    url(r'^create/z_sale/$', SaleZeroCreate.as_view(), name="create_zero-sale"),
    url(r'^validate/sale/$', ValidateSaleView.as_view(), name="validate_sale_card"),
    url(r'^validate/sale/card/$', ValidateSaleCardView.as_view(), name="validate_sale"),
    url(r'^my-sale/(?P<sale>[-\w]+)/(?P<status>[-\w]+)/$', ListMyTicketsView.as_view(), name="pay-response"),
    url(r'^api/sendTicket/$', SendTicketAPIView.as_view(), name='send_ticket'),
    url(r'^api/resent/$', ReSentAPIView.as_view(), name='resent_confirm'),
    url(r'^send_ticket/$', send_ticket, name="send_ticket"),
]
