from django.core.mail import EmailMessage
from django.template import loader, Context

from apps.accounts.models import User
from apps.core.helpers.pdf import ExportPDF
from apps.payment.models import Sale
from fuzzpass.celery import app as celery_app
from apps.web.models import LogsEvent


@celery_app.task
def send_email_sale(sale_id, owner_id):
    sale = Sale.objects.get(pk=sale_id)
    owner = User.objects.get(pk=owner_id)
    tickets = sale.tickets_sales.all()
    ticket = tickets.first()
    event = ticket.event
    quantity = tickets.count()
    data = {
        'sale': sale,
        'event': event.name,
        'event_object': event,
        'currency_symbol': event.currency_symbol,
        'ticket': ticket.zone,
        'user': owner,
        'quantity': quantity,
        'total_amount': sale.amount
        # 'domain': domain
    }

    template = loader.get_template('layouts/mailing/sale.html')
    html = template.render(Context(data))
    if event.invitation:
        subject_user = 'Detalle de tu reserva'
    else:
        subject_user = 'Detalle de tu compra'

    from_email = 'Festival de cine de lima <reservas@festivaldelima.com>'
    subject_organizer = 'Se ha vendido una entrada!'

    emails = [owner.email, 'team@fuzzpass.com']
    # emails = ['danielhuamani15@gmail.com']
    if event.organizer.user:
        message_user = EmailMessage(
            subject_organizer,
            html,
            from_email,
            [event.organizer.user.email]
        )
        message_user.content_subtype = "html"
        message_user.send(fail_silently=False)

    export_pdf = ExportPDF()
    pdf = export_pdf.create(sale)

    for to_email in emails:
        try:
            message_user = EmailMessage(
                subject_user, html, from_email, [to_email])
            message_user.content_subtype = "html"
            message_user.attach_file(pdf)
            message_user.send(fail_silently=False)
        except Exception as e:
            print(e)
            # LogsEvent.objects.create(
            #     title='Culqui-error envio email ticket',
            #     data=str(e),
            #     message='Proceso de envio de email luego de pagar',
            #     nivel='E',
            #     type_log='C',
            #     user=owner
            # )
