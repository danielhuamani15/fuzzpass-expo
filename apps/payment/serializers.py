import base64
import os
from io import BytesIO
from smtplib import SMTPException

import qrcode
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template import loader, Context
from rest_framework import serializers, status
from rest_framework.response import Response

from apps.payment.models import Ticket


def link_callback(uri, rel):
    sUrl = settings.STATIC_URL
    sRoot = settings.STATIC_ROOT
    mUrl = settings.MEDIA_URL
    mRoot = settings.MEDIA_ROOT
    path = ''
    if uri.startswith(mUrl):
        path = os.path.join(mRoot, uri.replace(mUrl, ""))
    elif uri.startswith(sUrl):
        path = os.path.join(sRoot, uri.replace(sUrl, ""))
    if not os.path.isfile(path):
        raise Exception('media URI must start with %s or %s' % (sUrl, mUrl))
    return path


class SendTicketSerializer(serializers.Serializer):
    name = serializers.CharField(required=True)
    email = serializers.EmailField(required=True)
    email2 = serializers.EmailField(required=True)
    ticketObj = serializers.IntegerField(required=True)

    def send_mail(self, ticket, pre_uni, user):
        email = self.validated_data.get("email")
        addressee = self.validated_data.get("name")
        request = self.context.get("request")
        domain = get_current_site(request).domain
        data = {
            'ticket': ticket,
            'name': addressee,
            'user': user,
            'domain': domain,
            'pre_uni': pre_uni,
            'currency_symbol': ticket.event.currency_symbol
        }
        if email == request.user.email:
            template = loader.get_template('layouts/mailing/myticket.html')
            subject_user, from_email = 'Tu ticket Fuzz Pass', 'Fuzz Pass <tickets@fuzzpass.com>'
        else:
            template = loader.get_template('layouts/mailing/ticket.html')
            subject_user, from_email = 'Tu ticket Fuzz Pass enviado por ' + user.get_full_name(), 'Fuzz Pass <tickets@fuzzpass.com>'
        html = template.render(Context(data))
        message_user = EmailMessage(subject_user, html, from_email, [email])
        message_user.content_subtype = "html"
        try:
            if message_user.send(fail_silently=False):
                obj_ticket = Ticket.objects.get(pk=ticket.id)
                obj_ticket.status = 'e'
                obj_ticket.save()
            else:
                pass

        except SMTPException:
            return Response(status=status.HTTP_400_BAD_REQUEST)
