# -*- coding: utf-8 -*-
import datetime
import json
from io import BytesIO

import qrcode
from braces.views import LoginRequiredMixin
from django.contrib.sites.shortcuts import get_current_site
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.template import loader, Context
from django.views.generic import DetailView, ListView, View
from django.views.generic.edit import CreateView
from django.conf import settings
from django.core.urlresolvers import reverse
from django.shortcuts import redirect
import culqipy
from rest_framework import generics, status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from apps.seats.utils import reserve_seats, update_seats_status
from apps.seats.models import ZoneSeat
from apps.payment.serializers import SendTicketSerializer
from fuzzpass.utils.mixins import ConfirmMailRequireMixin
from ..accounts.models import User
from ..payment.models import Sale, Ticket
from ..web.models import Event, Zone
from apps.payment.tasks import send_email_sale
from apps.web.models import LogsEvent
from .culqui import CulqiPayment
culqipy.public_key = settings.CULQI_PUBLIC_KEY
culqipy.secret_key = settings.CULQI_SECRET_KEY


class BuyDetail(LoginRequiredMixin, DetailView):
    template_name = 'pages/saleCust.html'
    context_object_name = 'event'
    model = Event

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if request.user.is_authenticated():
            if not request.user.confirm:
                return redirect(reverse('webs:user_confirm'))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BuyDetail, self).get_context_data(**kwargs)
        context['CULQI_PUBLIC_KEY'] = settings.CULQI_PUBLIC_KEY
        return context

    def get_object(self, queryset=None):
        return get_object_or_404(
            Event.objects.all(),
            slug_name__iexact=self.kwargs.get('slug_name')
        )


class BuyDetailRegisterCard(LoginRequiredMixin, DetailView):
    template_name = 'pages/saleCustRegisterCard.html'
    context_object_name = 'event'
    model = Event

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if request.user.is_authenticated():
            if not request.user.confirm:
                return redirect(reverse('webs:user_confirm'))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(BuyDetailRegisterCard, self).get_context_data(**kwargs)
        context['CULQI_PUBLIC_KEY'] = settings.CULQI_PUBLIC_KEY
        context['cards'] = self.request.user.user_culqui_card.all()
        return context

    def get_object(self, queryset=None):
        return get_object_or_404(
            Event.objects.all(),
            slug_name__iexact=self.kwargs.get('slug_name')
        )

class BuyInfluenceDetail(LoginRequiredMixin, DetailView):
    template_name = 'pages/saleCust-inf.html'
    context_object_name = 'event'
    model = Event

    def get_context_data(self, **kwargs):
        context = super(BuyInfluenceDetail, self).get_context_data(**kwargs)
        context['person'] = settings.HASHIDS.decode(self.kwargs.get('uid_inf'))[0]
        return context

    def get_object(self, queryset=None):
        buy_inf_event = self.kwargs.get('uid_evn')
        buy_inf_event_pk = settings.HASHIDS.decode(buy_inf_event)[0]
        return get_object_or_404(
            Event.objects.all(),
            pk=buy_inf_event_pk
        )


class SaleCreate(ConfirmMailRequireMixin, CreateView):
    model = Sale

    def post(self, request, *args, **kwargs):
        description = 'Comprando desde Fuzzpass con Culqi.'
        status = request.POST['status']
        pk_event = request.POST['event']
        zone_id = int(request.POST['zone'])
        quantity = int(request.POST['quantity'])
        source_type = request.POST['source_type']
        source_person = request.POST['source_person']
        selected_seats = request.POST['selected_seats']
        event = Event.objects.get(pk=pk_event)
        money = event.currency

        client = request.user
        LogsEvent.objects.create(
            title='SALE CREATE DATA',
            data=str(request.POST) + client.get_full_name() + ' ' + client.email,
            message='data que se envia en el post sale create',
            nivel='I',
            user=client
        )
        if not source_person == '':
            source_person_obj = User.objects.get(id=source_person)
        else:
            source_person_obj = User.objects.none()

        zone = Zone.objects.get(id=zone_id)
        total_amount = zone.get_total_amount(quantity)

        if selected_seats:
            selected_seats = selected_seats.split(',')
            seats = reserve_seats(zone, selected_seats, source_person_obj)
        else:
            seats = [None] * quantity
        LogsEvent.objects.create(
            title='SALE CREATE Seats elegidos',
            data=str(seats) ,
            message='data seats elegidos',
            nivel='I',
            user=client
        )
        sale_obj = Sale()
        sale_obj.amount = total_amount
        sale_obj.description = description
        sale_obj.money = money
        sale_obj.client_id = client.id
        sale_obj.status = status
        sale_obj.source_type = source_type
        if source_person_obj:
            sale_obj.source_person = source_person_obj
        sale_obj.save()
        for wish in event.wishes.all():
            if wish.user.id == client.id:
                wish.status = False
                wish.save()

        price = zone.get_total_price(1)
        data = {
            'sale': str(total_amount),
            'source_person_obj': source_person_obj
        }
        LogsEvent.objects.create(
            title='SALE CREATE SALE',
            data=str(data) ,
            message='data luego de recorrer wish',
            nivel='I',
            user=client
        )
        for seat, i in zip(seats, range(0, quantity)):
            ticket = Ticket()
            ticket.owner_id = client.id
            ticket.event = event
            if source_person_obj:
                ticket.influence = source_person_obj
            if seat is not None:
                ticket.seat = seat[0]
                if seat[1]:
                    ticket.seat_section = seat[1]
                else:
                    ticket.seat_section = zone.name

            else:
                ticket.seat_section = zone.name
            ticket.sales = sale_obj
            ticket.zone = zone
            ticket.price = price
            ticket.save()

            ticket.code = ticket.get_code()
            data = {
                'seat': str(seat),
                'ticker': ticket.code
            }
            LogsEvent.objects.create(
                title='SALE CREATE for tickes',
                data=str(data),
                message='Entro al bucle For para generar los tickets',
                nivel='I',
                user=client
            )
            qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.ERROR_CORRECT_L,
                box_size=10,
                border=4
            )

            data = '{}'.format(ticket.code)

            qr.add_data(data)
            qr.make(fit=True)
            img = qr.make_image()
            buffer = BytesIO()
            img.save(buffer)
            filename = 'ticket-%s.png' % ticket.id
            filebuffer = InMemoryUploadedFile(
                buffer, None, filename, 'image/png', None, None)
            ticket.qr.save(filename, filebuffer)
            ticket.save()

        culqi = {
            "numero_pedido": sale_obj.uid + "_" + str(datetime.date.today().toordinal()),
            "zone_name": zone.name,
            "moneda": sale_obj.money,
            "monto": int(float(sale_obj.amount) * 100),
            "descripcion": sale_obj.description,
            "correo_electronico": client.email,
            "cod_pais": 'PE',
            "ciudad": client.city,
            "direccion": client.address,
            "num_tel": 999999999,
            "id_usuario_comercio": client.uid,
            "nombres": client.first_name,
            "apellidos": client.last_name
        }
        LogsEvent.objects.create(
            title='SALE CREATE SE TERMINO EL PROCESO CULQUI',
            data=str(culqi) ,
            message='final del proceso',
            nivel='I',
            user=client
        )
        date_remaining = sale_obj.date_created + datetime.timedelta(minutes=10)
        data = {
            'resume': {
                'zone_name': zone.name,
                'quantity': quantity,
                'total_commission': float(zone.get_total_commission(quantity)),
                'total_price': float(zone.get_total_price(quantity)),
                'total_amount': float(total_amount),
                'date_remaining': date_remaining.strftime("%Y/%m/%d %H:%M:%S")
            },
            'informacion_venta': culqi
        }

        return HttpResponse(json.dumps(data), content_type="application/json", status=200)


class SaleZeroCreate(LoginRequiredMixin, CreateView):
    model = Sale

    def post(self, request, *args, **kwargs):
        description = 'Comprando desde Fuzzpass con Culqi.'
        status = request.POST['status']
        pk = request.POST['cliente']
        pk_event = request.POST['event']
        quantity = int(request.POST['quantity'])
        zone_id = int(request.POST['zone'])

        event = Event.objects.get(pk=pk_event)
        client = User.objects.get(pk=pk)
        zone = Zone.objects.get(id=zone_id)
        total_amount = zone.get_total_amount(quantity)
        money = event.currency

        zeros = Sale(amount=total_amount, description=description, money=money, client=client, status=status)
        zeros.save()

        for wish in event.wishes.all():
            if wish.user.id == client.id:
                wish.status = False
                wish.save()

        for i in range(0, quantity):
            ticket = Ticket.objects.create(
                owner=client,
                event=event,
                influence=client,
                sales=zeros,
                zone=zone.name,
                status='c',
                price=0
            )
            ticket.code = ticket.get_code()
            qr = qrcode.QRCode(
                version=1,
                error_correction=qrcode.ERROR_CORRECT_L,
                box_size=10,
                border=4
            )

            data = '{}'.format(ticket.code)

            qr.add_data(data)
            qr.make(fit=True)
            img = qr.make_image()
            buffer = BytesIO()
            img.save(buffer)
            filename = 'ticket-%s.png' % ticket.id
            filebuffer = InMemoryUploadedFile(buffer, None, filename, 'image/png', None, None)
            ticket.qr.save(filename, filebuffer)
            ticket.save()

        total_venta = zeros.amount
        domain = get_current_site(request).domain
        data = {
            'sale': zeros,
            'user': client,
            'event': event.name,
            'ticket': zone.name,
            'total': total_amount,
            'total_venta': total_venta,
            'domain': domain
        }
        template = loader.get_template('layouts/mailing/sale.html')
        html = template.render(Context(data))
        subject_user, from_email = 'Detalle de tu compra', 'Fuzz Pass <tickets@fuzzpass.com>'
        subject_organizer = 'Se ha vendido una entrada!'

        emails = [client.email, 'team@fuzzpass.com']

        if event.organizer.user:
            message_user = EmailMessage(
                subject_organizer,
                html,
                from_email,
                [event.organizer.user.email]
            )
            message_user.content_subtype = "html"
            message_user.send(fail_silently=False)

        for to_email in emails:
            message_user = EmailMessage(
                subject_user, html, from_email, [to_email])
            message_user.content_subtype = "html"
            message_user.send(fail_silently=False)

        data = {
            "sale": zeros.pk,
            "status_sale": 1
        }

        return HttpResponse(json.dumps({"data": data}), content_type="application/json", status=200)


class ValidateSaleView(View):
    def post(self, request, *args, **kwargs):
        pk = request.POST['user']
        sale_status = request.POST['status_sale']
        ticket_status = request.POST['status_ticket']
        owner = User.objects.get(pk=pk)
        sale_id = request.POST['sale']
        token = request.POST['token']
        save_card = request.POST.get('save_card')
        sale_pk = settings.HASHIDS.decode(sale_id)[0]
        LogsEvent.objects.create(
            title='Inicio del Proceso de Culqui',
            data=str(request.POST),
            message='data que se envia en el post culqui',
            type_log='C',
            nivel='I',
            user=owner
        )
        obj_sale = Sale.objects.get(pk=sale_pk)
        tickets = Ticket.objects.filter(owner=owner, sales=obj_sale)
        data = {
            'amount': int(float(obj_sale.amount) * 100),
            'capture': True,
            'currency_code': tickets[0].event.currency,
            'description': tickets[0].event.name + '' + tickets[0].zone.name,
            'email': owner.email,
            'installments': 0,
            'metadata': {},
            'source_id': token
        }
        print(save_card, 'save_card')
        if save_card == 'true':
            culqui_pay = CulqiPayment()
            status, result = culqui_pay.initialize(owner , token)
            if status:
                return HttpResponse(
                json.dumps({"user_message": result}),
                content_type="application/json", status=400)
            charge = culqui_pay.create_charge(data)
        else:
            charge = culqipy.Charge.create(data)

        LogsEvent.objects.create(
            title='Culqui - envio data',
            data=str(data),
            message='data que se envia en el post culqui',
            type_log='C',
            nivel='I',
            user=owner
        )
        LogsEvent.objects.create(
            title='Culqui - respuesta data',
            data=str(charge),
            message='data que retorna culqui',
            type_log='C',
            nivel='I',
            user=owner
        )

        if charge['object'] == 'error':
            return HttpResponse(
                json.dumps({"user_message": charge['user_message']}),
                content_type="application/json", status=400)
        else:
            tickets.update(status=ticket_status)
            Sale.objects.filter(pk=sale_pk).update(status=sale_status)
            domain = get_current_site(request).domain

            update_seats_status(tickets, owner)

            send_email_sale.apply_async((), {
                'sale_id': obj_sale.id,
                'owner_id': owner.id,
                'domain': domain
            })

        return HttpResponse(json.dumps({"sale": sale_pk, "status_sale": 1}), content_type="application/json", status=200)


class ValidateSaleCardView(View):
    def post(self, request, *args, **kwargs):
        pk = request.POST['user']
        card_id = request.POST['card_id']
        sale_status = request.POST['status_sale']
        ticket_status = request.POST['status_ticket']
        owner = User.objects.get(pk=pk)
        card = owner.user_culqui_card.filter(id=card_id).first()
        sale_id = request.POST['sale']
        sale_pk = settings.HASHIDS.decode(sale_id)[0]
        LogsEvent.objects.create(
            title='Inicio del Proceso de Culqui Card',
            data=str(request.POST),
            message='data que se envia en el post culqui Card',
            type_log='C',
            nivel='I',
            user=owner
        )
        obj_sale = Sale.objects.get(pk=sale_pk)
        tickets = Ticket.objects.filter(owner=owner, sales=obj_sale)
        data = {
            'amount': int(float(obj_sale.amount) * 100),
            'capture': True,
            'currency_code': tickets[0].event.currency,
            'description': tickets[0].event.name + '' + tickets[0].zone.name,
            'email': owner.email,
            'installments': 0,
            'metadata': {},
            'source_id': card.id_culqui
        }
        culqui_pay = CulqiPayment()
        charge = culqui_pay.create_charge(data)
        LogsEvent.objects.create(
            title='Culqui Card - envio data',
            data=str(data),
            message='data que se envia en el post culqui',
            type_log='C',
            nivel='I',
            user=owner
        )
        LogsEvent.objects.create(
            title='Culqui Card - respuesta data',
            data=str(charge),
            message='data que retorna culqui',
            type_log='C',
            nivel='I',
            user=owner
        )

        if charge['object'] == 'error':
            return HttpResponse(
                json.dumps({"user_message": charge['user_message']}),
                content_type="application/json", status=400)
        else:
            tickets.update(status=ticket_status)
            Sale.objects.filter(pk=sale_pk).update(status=sale_status)
            domain = get_current_site(request).domain

            update_seats_status(tickets, owner)

            send_email_sale.apply_async((), {
                'sale_id': obj_sale.id,
                'owner_id': owner.id,
                'domain': domain
            })

        return HttpResponse(json.dumps({"sale": sale_pk, "status_sale": 1}), content_type="application/json", status=200)


class ListMyTicketsView(LoginRequiredMixin, ListView):
    template_name = 'pages/my-buy_tickets.html'
    context_object_name = 'tickets'
    model = Ticket
    sale_pk = None

    def get_context_data(self, **kwargs):
        context = super(ListMyTicketsView, self).get_context_data(**kwargs)
        context['status'] = self.kwargs['status']
        obj_sale = Sale.objects.get(id=self.sale_pk)
        total = float(obj_sale.amount)
        cantidad = Ticket.objects.filter(owner=self.request.user).filter(
            sales=self.sale_pk).exclude(status='f').count()
        precio_uni = total / cantidad
        domain = get_current_site(self.request).domain
        context['precio_uni'] = precio_uni
        context['domain'] = domain
        return context

    def get_queryset(self):
        sale_id = self.kwargs['sale']
        self.sale_pk = settings.HASHIDS.decode(sale_id)[0]
        tickets = Ticket.objects.filter(owner=self.request.user).filter(
            sales_id=self.sale_pk).exclude(status='f')
        return tickets


class SendTicketAPIView(generics.GenericAPIView):
    serializer_class = SendTicketSerializer
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if serializer.data['email'] != serializer.data['email2']:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        obj_ticket = Ticket.objects.get(pk=int(request.POST['ticketObj']))
        obj_ticket.full_name = request.POST['name']
        obj_ticket.email = request.POST['email']
        obj_ticket.sent_date = request.POST['sent_date']
        obj_ticket.save()
        total = float(obj_ticket.sales.amount)
        cantidad = Ticket.objects.filter(owner=self.request.user).filter(sales=obj_ticket.sales).exclude(status='f').count()
        precio_uni = total / cantidad
        # precio_uni = 200
        user = request.user
        serializer.send_mail(obj_ticket, precio_uni, user)
        return Response(status=status.HTTP_200_OK)


class ReSentAPIView(generics.GenericAPIView):
    serializer_class = SendTicketSerializer
    authentication_classes = (SessionAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        user = request.user
        print(user, 'user')
        token, created = Token.objects.get_or_create(user=user)
        domain = get_current_site(self.request).domain
        data = {'user': user, 'token': token, 'domain': domain}
        template = loader.get_template('layouts/mailing/welcome.html')
        html = template.render(dict(data))
        subject_user, from_email = 'Bienvenido a Fuzz Pass', 'Fuzz pass <tickets@fuzzpass.com>'
        message_user = EmailMessage(subject_user, html, from_email, [user.email])
        message_user.content_subtype = "html"
        message_user.send(fail_silently=False)
        return Response(status=status.HTTP_200_OK)


class ChooseSeats(LoginRequiredMixin, DetailView):
    template_name = 'seats/chooseSeats.html'
    model = Event
    _section = None

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if request.user.is_authenticated():
            if not request.user.confirm:
                return redirect(reverse('webs:user_confirm'))
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        zone_id = self.kwargs.get('zone_id')
        zone = get_object_or_404(Zone, id=zone_id)
        section = zone.section_set.last()
        self._section = section
        reserved_seats = ZoneSeat.objects.select_related('seat').filter(
            zone=zone,
            status=ZoneSeat.STATUS_RESERVED
        )
        bought_seats = ZoneSeat.objects.select_related('seat').filter(
            zone=zone,
            status=ZoneSeat.STATUS_BOUGHT
        )

        context = super(ChooseSeats, self).get_context_data(**kwargs)
        context['zone'] = zone
        context['section'] = section
        context['reserved_seats'] = reserved_seats
        context['bought_seats'] = bought_seats

        return context

    def get_object(self, queryset=None):
        slug_name = self.kwargs.get('slug_name')
        return get_object_or_404(
            Event,
            slug_name__iexact=slug_name
        )


def send_ticket(request):
    ticket = Ticket.objects.last()
    send_email_sale(ticket.sales.id, ticket.owner_id)
    return HttpResponse(
        json.dumps({}), content_type="application/json", status=200)