from django.conf import settings
import culqipy
from ..accounts.models import User
from .models import CulquiCustomer, CulquiCard

culqipy.public_key = settings.CULQI_PUBLIC_KEY
culqipy.secret_key = settings.CULQI_SECRET_KEY

class CulqiPayment(object):

    # def __init__(self, user=None, token_id=None):
    #     self.token_id = token_id
    #     self.user = user
    #     self.customer = user.user_costumer
    #     self.shipping = user.user_costumer.customer_shippings.first()

    def initialize(self, user, token_id):
        self.user = user
        customer_error = False
        try:
            user_culqui = self.user.user_culqui
        except:
            customer_error, user_culqui = self.create_user(user)
        print(user_culqui)
        if customer_error:
            return customer_error, user_culqui
        else:
            error, card_culqui = self.get_or_create_card(
                user_culqui.id_culqui, token_id)
            return error, card_culqui

    def create_charge(self, data):
        result_charge = culqipy.Charge.create(data)
        return result_charge

    def setCustomers(self):
        result = self.list_user()
        data = result.get('data')
        error = []
        print(data, 'datadatadata', result)
        if data:
            for cus in data:
                email = cus.get('email')
                id = cus.get('id')
                print(id, email)
                try:
                    if not CulquiCustomer.objects.filter(
                        id_culqui=id).exists() and User.objects.filter(email=email).exists():
                        user = User.objects.get(email=email)
                        creation_date = cus.get('creation_date')
                        customer_culqui = CulquiCustomer()
                        customer_culqui.customer = user
                        customer_culqui.id_culqui = id
                        customer_culqui.creation_date = creation_date
                        customer_culqui.save()
                except Exception as e:
                    error.append(str(e) + ' ' + email)
        print(error)

    def list_user(self):
        result_customer = culqipy.Customer.list()
        return result_customer

    def create_user(self, user):
        result_customer = culqipy.Customer.create({
            'country_code': "PE",
            'email': user.email,
            'first_name': user.last_name,
            'last_name': user.last_name,
            'phone_number': user.phone,
            'address': user.address,
            'address_city':  '{0}'.format(user.city),
        })
        if result_customer.get('object') == 'error':
            return True, (result_customer.get('merchant_message')  + '(' + self.user.email + ')')
        id = result_customer.get('id')
        creation_date = result_customer.get('creation_date')
        customer_culqui = CulquiCustomer()
        customer_culqui.customer = user
        customer_culqui.id_culqui = id
        customer_culqui.creation_date = creation_date
        customer_culqui.save()
        return False, customer_culqui

    def delete_card(self, card_id):
        result_card = culqipy.Card.delete(card_id)
        return result_card

    def get_or_create_card(self, customer_id, token_id):
        result_card = culqipy.Card.create({
            'customer_id': customer_id,
            'token_id': token_id
        })
        if result_card.get('object') == 'error':
            return True, result_card.get('merchant_message')
        try:
            card_culqui = CulquiCard.objects.get(id_culqui=result_card.get('id'))
        except Exception as e:
            card_culqui = CulquiCard()
            card_culqui.id_culqui_customer = result_card.get('customer_id')
            card_culqui.id_culqui = result_card.get('id')
            card_culqui.card_number = result_card.get('source').get('card_number')
            card_culqui.card_brand = result_card.get('source').get('iin').get('card_brand')
            card_culqui.card_type = result_card.get('source').get('iin').get('card_type')
            card_culqui.email = result_card.get('source').get('email')
            card_culqui.customer = self.user
            card_culqui.is_selected = True
            card_culqui.save()
        self.user.user_culqui_card.filter(
                            is_selected=True).exclude(
                            id=card_culqui.id).update(is_selected=False)
        return False, card_culqui
