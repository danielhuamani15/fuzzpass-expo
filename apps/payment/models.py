# coding:utf-8
from enum import Enum

import qrcode
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import models
from django.utils.six import StringIO

from django.conf import settings
from ..accounts.models import User
from fuzzpass.utils.mixins import UidMixin


class Sale(UidMixin, models.Model):
    class Status(Enum):
        i = "Invalido"
        p = "PreValido"
        v = "Valido"

    class Sources(Enum):
        f = "Fuzzpass"
        i = "Influence"

    money = models.CharField(max_length=3, help_text="Ingrese el código de la moneda", verbose_name="Código de moneda",
                             default='PEN')
    amount = models.DecimalField(max_digits=8, decimal_places=2, help_text="Ingrese el monto", verbose_name="Monto")
    description = models.TextField(max_length=80, help_text="Ingrese una descripción", verbose_name="Descripción")
    status = models.CharField(max_length=1, blank=True, choices=[(item.name, item.value) for item in Status],
                              default=Status.i.name)
    client = models.ForeignKey(User, related_name="sales")
    date_created = models.DateTimeField(auto_now_add=True)
    source_type = models.CharField(max_length=1, choices=[(item.name, item.value) for item in Sources],
                                   default=Sources.i.name, null=True, blank=True)
    source_person = models.ForeignKey(User, related_name="sales_source_person", default='', null=True, blank=True)
    pdf = models.FileField(upload_to='tickets/pdf', null=True, blank=False)

    class Meta:
        verbose_name = "Venta"
        verbose_name_plural = "Ventas"


class Ticket(UidMixin, models.Model):
    class Status(Enum):
        f = "borrador"
        c = "comprado"
        e = "enviado"
        u = "usado"

    code = models.CharField(max_length=8, blank=True, default='', verbose_name='Código del ticket')
    qr = models.ImageField(verbose_name='Qr', upload_to='tickets/qrcode', null=True, blank=True)
    email = models.EmailField(max_length=150)
    dni = models.CharField(max_length=8)
    full_name = models.CharField(max_length=200, default='')
    status = models.CharField(max_length=1, blank=True, choices=[(item.name, item.value) for item in Status],
                              default=Status.f.name)
    event = models.ForeignKey('web.Event', related_name='tickets')
    zone = models.ForeignKey('web.Zone', blank=True, null=True)
    owner = models.ForeignKey(User, related_name='tickets')
    influence = models.ForeignKey(User, related_name='tickets_influence', blank=True, null=True)
    description = models.CharField(max_length=45, verbose_name='description', null=True)
    price = models.DecimalField(max_digits=8, decimal_places=2, help_text="Valor Unitario del ticket", verbose_name="Valor", default=0)
    sales = models.ForeignKey(Sale, related_name='tickets_sales', null=True)
    sent_date = models.DateTimeField(null=True)
    seat = models.CharField(max_length=20, blank=True)
    seat_section = models.CharField(max_length=50, blank=True)
    is_invitation = models.BooleanField(default=False)

    def get_code(self):
        return settings.HASHIDS.encode(self.id)

    def generate_qrcode(self):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=6,
            border=0,
        )
        qr.add_data(self.get_absolute_url())
        qr.make(fit=True)

        img = qr.make_image()

        buffer = StringIO.StringIO()
        img.save(buffer)
        filename = 'events-%s.png' % (self.id)
        filebuffer = InMemoryUploadedFile(
            buffer, None, filename, 'image/png', buffer.len, None)
        self.qr.save(filename, filebuffer)

    def __str__(self):
        return self.code


class CulquiCustomer(models.Model):
    created = models.DateTimeField('created', auto_now_add=True)
    modified = models.DateTimeField('modified', auto_now=True)
    is_active = models.BooleanField('is active', default=True)
    customer = models.OneToOneField('accounts.User', on_delete=models.CASCADE,
                                    related_name='user_culqui')
    id_culqui = models.CharField(max_length=255, unique=True)
    creation_date = models.CharField(max_length=255)

    class Meta:
        verbose_name = "CulquiCustomer"
        verbose_name_plural = "CulquiCustomers"

    def __str__(self):
        return self.id_culqui


class CulquiCard(models.Model):
    created = models.DateTimeField('created', auto_now_add=True)
    modified = models.DateTimeField('modified', auto_now=True)
    is_active = models.BooleanField('is active', default=True)
    customer = models.ForeignKey('accounts.User', on_delete=models.CASCADE,
                                    related_name='user_culqui_card')
    id_culqui = models.CharField(max_length=255, unique=True)
    id_culqui_customer = models.CharField(max_length=255)
    card_number = models.CharField(max_length=255)
    card_brand = models.CharField(max_length=255)
    card_type = models.CharField(max_length=255)
    is_selected = models.BooleanField(default=False)

    class Meta:
        verbose_name = "CulquiCard"
        verbose_name_plural = "CulquiCard"

    def __str__(self):
        return self.card_number
