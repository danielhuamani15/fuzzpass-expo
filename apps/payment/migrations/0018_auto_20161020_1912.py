# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-10-20 19:12
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0017_auto_20161019_0844'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sale',
            name='source_person',
            field=models.ForeignKey(blank=True, default='', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='sales_source_person', to=settings.AUTH_USER_MODEL),
        ),
    ]
