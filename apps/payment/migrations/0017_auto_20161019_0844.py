# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-10-19 08:44
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payment', '0016_auto_20161017_1719'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sale',
            name='source_type',
            field=models.CharField(blank=True, choices=[('f', 'Fuzzpass'), ('i', 'Influence')], default='i', max_length=1, null=True),
        ),
    ]
