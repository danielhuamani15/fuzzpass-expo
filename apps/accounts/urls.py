from django.contrib.auth import views
from django.core.urlresolvers import reverse_lazy

from .views import ProfileView, LogoutView, LoginWebView, UserRegisterView, \
    TestRegisterView, TestLoginView, \
    LoginPlaneWebView, ProfileUpdateView, ListWishView, LoginAPIView, UserConfirmWebView, UserFromInfluenceConfirmWebView, \
    LoginAndSingUpView
from django.conf.urls import url

urlpatterns = [
    url(r'^wish-list/$', ListWishView.as_view(), name='wish-list'),
    url(r'^perfil/$', ProfileView.as_view(), name='profile'),
    url(r'^profile/update/$', ProfileUpdateView.as_view(), name='profile-update'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^api/login/$', LoginAPIView.as_view(), name='api-login'),
    url(r'^login-plane/$', LoginPlaneWebView.as_view(), name='login-plane'),
    url(r'^confirm/user/(?P<token>[0-9A-Za-z_\-]+)/$',
        UserConfirmWebView.as_view(), name='user-confirm'),
    url(r'^confirm/userFromInfluence/(?P<token>[0-9A-Za-z_\-]+)/(?P<influence>[-\w]+)/(?P<event>[-\w]+)/$',
        UserFromInfluenceConfirmWebView.as_view(), name='user-confirm'),
    url(r'^register/$', UserRegisterView.as_view(), name='register'),
    url(r'^test-register/$', TestRegisterView.as_view(), name="test-register"),
    url(r'^test-login/$', TestLoginView.as_view(), name="test-login"),
    url(r'^password-reset/$', views.password_reset,
        {"post_reset_redirect": reverse_lazy('accounts:password_reset_done'), 'from_email': 'reservas@festivaldelima.com', 'subject_template_name': 'registration/reset_subject.txt',
         'html_email_template_name': 'registration/password_reset_html_email.html'},
        name='password_reset'),
    url(r'^password_reset/done/$', views.password_reset_done,
        name='password_reset_done'),
    url(
        r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.password_reset_confirm,
        {"post_reset_redirect": reverse_lazy(
            'accounts:password_reset_complete')},
        name='password_reset_confirm'),
    url(
        r'^reset/done/$',
        views.password_reset_complete,
        name='password_reset_complete'
    ),
    url(
        r'^iniciar-sesion/$',
        LoginAndSingUpView.as_view(),
        name='login-signup'
    ),
]
