from enum import Enum
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser)
from django.contrib.auth.models import PermissionsMixin
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.db import models
from django.template import loader
from easy_thumbnails.fields import ThumbnailerImageField
from fuzzpass.utils.mixins import UidMixin
from rest_framework.authtoken.models import Token


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser,
                     **extra_fields):
        user = self.model(email=email, is_active=True,
                          is_staff=is_staff, is_superuser=is_superuser,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, True, True,
                                 **extra_fields)


class Musicals(models.Model):
    name = models.CharField(max_length=15, verbose_name="Nombre del estilo de musica")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = 'Estilos musicales'


class User(UidMixin, AbstractBaseUser, PermissionsMixin):
    class Gender(Enum):
        m = "Masculino"
        f = "Femenino"

    class Modes(Enum):
        n = "Normals"
        i = "Influencer"
        p = "Producers"
        a = "AdminEvent"

    email = models.EmailField(max_length=150, unique=True)
    country_code = models.CharField(max_length=2, help_text='Ingrese el código del país', default="PE")
    city = models.CharField(max_length=30, help_text='Ingrese el nombre de la ciudad', default="lima")
    address = models.CharField(max_length=100, help_text='Ingrese la dirección', default="Av. Universitaria")
    phone = models.CharField(max_length=18, blank=True, help_text='Ejm: 965 789 456')
    dni = models.CharField(max_length=8, blank=True)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    birthday = models.DateField(blank=True, null=True)
    photo = ThumbnailerImageField(upload_to='user/fronts', verbose_name='Portada', blank=True, null=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_modified = models.DateTimeField(auto_now=True)
    gender = models.CharField(max_length=1, blank=True, null=True, choices=[(item.name, item.value) for item in Gender])
    user_mode = models.CharField(max_length=1, blank=True, choices=[(item.name, item.value) for item in Modes], default=Modes.n.name)
    is_new = models.BooleanField(default=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    terms = models.BooleanField(default=False)
    event_admin = models.ForeignKey('web.Event', related_name='users', null=True, blank=True)
    confirm = models.BooleanField(default=False)
    USERNAME_FIELD = 'email'
    objects = UserManager()
    musicals = models.ManyToManyField(Musicals, related_name='users')

    class Meta:
        verbose_name = "Usuario"
        verbose_name_plural = "Usuarios"

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        self.first_name = self.first_name.title()
        self.last_name = self.last_name.title()
        return super(User, self).save(force_insert, force_update, using, update_fields)

    def get_short_name(self):
        return '{0} {1}'.format(self.first_name, self.last_name)

    def get_user_face(self):
        if self.social_auth.count() > 0:
            return True
        else:
            return False

    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def estilos_musicales(self):
        return "\n".join([p.name for p in self.musicals.all()])

    def send_mail_confirmation(self, user):
        token, created = Token.objects.get_or_create(user=user)
        domain = 'fuzzpass.com'
        data = {'user': user, 'token': token, 'domain': domain}
        template = loader.get_template('layouts/mailing/welcome.html')
        html = template.render(dict(data))
        subject_user, from_email = 'Bienvenido a Fuzz Pass', 'Fuzz pass <tickets@fuzzpass.com>'
        message_user = EmailMessage(subject_user, html, from_email, [user.email])
        message_user.content_subtype = "html"
        message_user.send(fail_silently=False)
