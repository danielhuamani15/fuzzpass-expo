# coding: utf-8
import operator
from datetime import date, timedelta, datetime
from braces.views import AnonymousRequiredMixin

from django.contrib import messages, auth
from class_based_auth_views.views import LoginView
from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.core.urlresolvers import reverse_lazy, reverse
from django.http.response import HttpResponse
from django.views.generic import View, ListView, TemplateView
from django.views.generic.edit import ModelFormMixin, UpdateView, CreateView
from django.shortcuts import redirect
from rest_framework import generics, status
from rest_framework.status import HTTP_404_NOT_FOUND

from apps.payment.models import Ticket
from apps.web.models import Wish
from .forms import CreateUserForm, UpdateUserForm, AuthenticationForm
from .serializers import *
from rest_framework.response import Response
from rest_framework.authtoken.models import Token


class AjaxTemplateMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if not hasattr(self, 'ajax_template_name'):
            split = self.template_name.split('.html')
            split[-1] = '_inner'
            split.append('.html')
            self.ajax_template_name = ''.join(split)

        if request.is_ajax():
            self.template_name = self.ajax_template_name

        return super(AjaxTemplateMixin, self).dispatch(request, *args, **kwargs)


class TestRegisterView(SuccessMessageMixin, AjaxTemplateMixin, AnonymousRequiredMixin, CreateView):
    model = User
    template_name = 'accounts/register-popup.html'
    form_class = CreateUserForm
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        self.object.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, self.object)
        return super(ModelFormMixin, self).form_valid(form)


class TestLoginView(SuccessMessageMixin, AjaxTemplateMixin, AnonymousRequiredMixin, LoginView):
    template_name = 'accounts/login-popup.html'
    form_class = AuthenticationForm
    authenticated_redirect_url = reverse_lazy('accounts:profile')

    def get_success_url(self):
        return reverse_lazy('webs:home')


class UserRegisterView(AnonymousRequiredMixin, CreateView):
    model = User
    form_class = CreateUserForm
    template_name = 'pages/home.html'
    success_url = reverse_lazy('webs:home')

    def form_valid(self, form):
        """
        If the form is valid, save the associated model.
        """
        self.object = form.save()
        self.object.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, self.object)
        return super(ModelFormMixin, self).form_valid(form)


class LoginWebView(AnonymousRequiredMixin, LoginView):
    template_name = 'pages/home.html'
    form_class = AuthenticationForm

    # def get_url(self):
    #     next_page = self.request.GET['next']
    #     return reverse(next_page)
    def get_success_url(self):
        return reverse_lazy('webs:home')


class LoginPlaneWebView(AnonymousRequiredMixin, LoginView):
    template_name = 'pages/login2.html'
    form_class = AuthenticationForm

    # authenticated_redirect_url = reverse_lazy('accounts:profile')

    def get_url(self):
        next_page = self.request.GET['next']
        return reverse('/')


class LogoutView(View):
    def get(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            auth.logout(self.request)
        return redirect(reverse_lazy('webs:events-View'))


class ProfileView(LoginRequiredMixin, TemplateView):
    model = User
    template_name = 'pages/profile.html'
    paginate_by = 6

    def get_context_data(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        context['list_wishes'] = Wish.objects.filter(user=self.request.user).filter(status=True)
        context['domain'] = get_current_site(self.request).domain
        context['tickets'] = Ticket.objects.filter(owner=self.request.user).exclude(status='f')
        # .filter(event__start_date__gte=date.today())
        good_memory_1 = Ticket.objects.filter(owner=self.request.user).filter(status='c')
        good_memory_2 = Ticket.objects.filter(owner=self.request.user).filter(status='e').filter(email=self.request.user.email)
        good_memory_3 = Ticket.objects.filter(owner=self.request.user).filter(status='u')
        good_memory = good_memory_1 | good_memory_2 | good_memory_3
        my__good_memories = good_memory.order_by('event').distinct('event')
        my__good_memories_ordered = sorted(my__good_memories, key=operator.attrgetter('event.start_date'), reverse=True)

        if my__good_memories:
            my__good_memory__validated = []
            now = datetime.now()
            for my__good_memory in my__good_memories_ordered:
                if now.date() >= my__good_memory.event.start_date.date() + timedelta(days=1) and now.hour >= 0:
                    my__good_memory__validated.append(my__good_memory)
            context['my_all_good_memories'] = my__good_memory__validated
        return context


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    template_name = 'pages/profileUpdate.html'
    context_object_name = 'user'
    form_class = UpdateUserForm
    success_url = reverse_lazy('accounts:profile')

    def get_object(self, queryset=None):
        return self.request.user

    def form_valid(self, form):
        self.object = form.save()
        messages.add_message(self.request, messages.SUCCESS, 'Edited successfully.')
        return super(ModelFormMixin, self).form_valid(form)


class ListWishView(LoginRequiredMixin, ListView):
    model = User
    template_name = 'pages/wish-list.html'

    def get_context_data(self, **kwargs):
        context = super(ListWishView, self).get_context_data(**kwargs)
        context['list_wishes'] = Wish.objects.filter(user=self.request.user).filter(status=True)
        return context


class UserConfirmWebView(View):
    def get(self, request, *args, **kwargs):
        try:
            Token.objects.get(key=kwargs.get("token"))
            token = Token.objects.get(key=kwargs.get("token"))
            user = User.objects.get(id=token.user.id)
            user.confirm = True
            user.save()
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
            return redirect('accounts:profile')
        except:
            return HttpResponse(HTTP_404_NOT_FOUND)


class UserFromInfluenceConfirmWebView(View):
    def get(self, request, *args, **kwargs):
        try:
            Token.objects.get(key=kwargs.get("token"))
            token = Token.objects.get(key=kwargs.get("token"))
            user = User.objects.get(id=token.user.id)
            user.confirm = True
            user.save()
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)
            influence = kwargs.get("influence")
            event = kwargs.get("event")
            return redirect('influences:inf-event_detail', influence, event)
        except:
            return HttpResponse(HTTP_404_NOT_FOUND)


class LoginAPIView(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, context={"request": request})
        serializer.is_valid(raise_exception=True)
        token, created = Token.objects.get_or_create(user=serializer.get_user())
        return Response({'token': token.key}, status=status.HTTP_200_OK)


class LoginAndSingUpView(AnonymousRequiredMixin, TemplateView):
    model = User
    template_name = 'pages/login_signup.html'
    form_class = CreateUserForm
    second_form_class = AuthenticationForm

    def get_context_data(self, **kwargs):
        context = super(LoginAndSingUpView, self).get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = self.form_class()
        if 'form2' not in context:
            context['form2'] = self.second_form_class()
        context['next'] = self.request.GET.get('next', '')
        return context

    def get_success_url(self):
        next_page = self.request.GET.get('next', '')
        if next_page:
            return next_page
        return reverse('webs:events-View')

    def get_form(self, form_class=None):
        return form_class(**self.get_form_kwargs())

    def get_form_kwargs(self):
        return {
                'data': self.request.POST,
                'files': self.request.FILES,
            }

    def post(self, request, *args, **kwargs):
        if 'form' in request.POST:
            form_class = self.form_class
            form_name = 'form'
        else:
            form_class = self.second_form_class
            form_name = 'form2'

        form = self.get_form(form_class)
        if form_name == 'form':
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.second_form_invalid(**{form_name: form})

        elif form_name == 'form2':
            if form.is_valid():
                if not form.cleaned_data.get('remember_me'):
                    request.session.set_expiry(0)
                return self.second_form_valid(form)
            else:
                return self.second_form_invalid(**{form_name: form})

    def form_valid(self, form):
        self.object = form.save()
        self.object.confirm = True
        self.object.save()
        self.object.backend = 'django.contrib.auth.backends.ModelBackend'
        login(self.request, self.object)
        return redirect(self.get_success_url())

    def second_form_valid(self, form):
        login(self.request, form.get_user())
        return redirect(self.get_success_url())

    def second_form_invalid(self, **kwargs):
        """
        The user has provided invalid credentials (this was checked in AuthenticationForm.is_valid()). So now we
        set the test cookie again and re-render the form with errors.
        """
        return self.render_to_response(self.get_context_data(**kwargs))
