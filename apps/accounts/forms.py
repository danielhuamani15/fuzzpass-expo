# coding: utf-8
import warnings
from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth import get_user_model, authenticate
from django.core.mail import EmailMessage
from django.template import loader, Context
from rest_framework.authtoken.models import Token

from .models import User, Musicals


class AuthenticationForm(forms.Form):
    """
    Base class for authenticating users. Extend this to get a form that accepts
    username/password logins.
    """
    email = forms.EmailField(
        required=True,
        widget=forms.TextInput(attrs={'placeholder': 'Email'})
    )
    password = forms.CharField(
        label="Password",
        widget=forms.PasswordInput(attrs={'placeholder': 'Contraseña'})
    )
    remember_me = forms.BooleanField(
        label='Recordar mi cuenta',
        required=False,
        widget=forms.CheckboxInput()
    )

    error_messages = {
        'invalid_login': u'Please enter a correct %(username)s and password.Note that both fields may be '
                         u'case-sensitive.',
        'inactive': u'This account is inactive',
    }

    def __init__(self, request=None, *args, **kwargs):
        """
        The 'request' parameter is set for custom auth use by subclasses.
        The form data comes in via the standard 'data' kwarg.
        """
        self.request = request
        self.user_cache = None
        super(AuthenticationForm, self).__init__(*args, **kwargs)

        # Set the label for the "username" field.
        UserModel = get_user_model()
        self.fields['email'].label = False
        self.fields['password'].label = False

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email and password:
            self.user_cache = authenticate(email=email,
                                           password=password)
            if self.user_cache is None:
                self.add_error('email',
                               'Por favor, introduzca correo correcto.')
                self.add_error('password',
                               'Por favor, introduzca password correcto.')
                # raise forms.ValidationError(
                #     self.error_messages['invalid_login'],
                #    code='invalid_login',
                #    params={'email': self.fields.get('email')},
                # )
            elif self.user_cache.confirm == False:
                self.add_error('email', "Confirmar el correo electrónico")
            elif not self.user_cache.is_active:
                self.add_error('email',
                               'Usuario inactivo.')
        return self.cleaned_data

    def check_for_test_cookie(self):
        warnings.warn("check_for_test_cookie is deprecated; ensure your login "
                      "view is CSRF-protected.", DeprecationWarning)

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

    @property
    def helper(self):
        helper = FormHelper()
        helper.form_tag = False  # don't render form DOM element
        helper.render_unmentioned_fields = True  # render all fields
        helper.field_class = 'col-md-12'
        # helper.form_show_labels = False
        return helper


class CreateUserForm(forms.ModelForm):

    first_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'placeholder': 'Nombres'})
    )
    last_name = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'placeholder': 'Apellidos'})
    )
    # dni = forms.CharField(
    #     required=True,
    #     widget=forms.TextInput(attrs={'placeholder': 'DNI'})
    # )
    email = forms.EmailField(
        required=True,
        widget=forms.TextInput(attrs={'placeholder': 'Email'})
    )
    influence = forms.CharField(required=False)
    event = forms.CharField(required=False)
    # gender = forms.ChoiceField(
    #     required=True,
    #     choices=[(item.name, item.value) for item in User.Gender],
    #     widget=forms.RadioSelect()
    # )
    terms = forms.BooleanField(
        error_messages={'required': 'Debe aceptar los Términos y Condiciones'},
        label="", required=True
    )
    password = forms.CharField(
        label="Password",
        widget=forms.PasswordInput(attrs={'placeholder': 'Contraseña'})
    )
    # birthday = forms.DateField(
    #     widget=forms.TextInput(attrs={
    #         'class': 'datepicker',
    #         'placeholder': 'Fecha de Nacimiento (DD/MM/AAAA)'
    #     })
    # )

    def __init__(self, *args, **kwargs):
        super(CreateUserForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].label = False
        self.fields['last_name'].label = False
        # self.fields['dni'].label = False
        # self.fields['birthday'].label = False
        # self.fields['gender'].label = False
        self.fields['email'].label = False
        self.fields['password'].label = False

    class Meta:
        model = User
        fields = ('first_name', 'last_name',
                  'email', 'password', 'terms', 'influence', 'event')

    def clean_password(self):
        if len(self.cleaned_data.get('password')) < 6:
            self.add_error('password', 'Mínimo 6 caracteres.')
        else:
            return self.cleaned_data.get('password')

    def clean_email(self):
        if User.objects.filter(email=self.cleaned_data['email']).exists():
            self.add_error('email', 'El email ya ha sido registrado.')
        else:
            return self.cleaned_data['email']

    def clean_terms(self):
        if self.cleaned_data["terms"] == u'on':
            self.add_error('terms', 'Debe aceptar los terminso y  condiciones.')
        else:
            return self.cleaned_data['terms']

    @property
    def helper(self):
        helper = FormHelper()
        helper.form_tag = False
        helper.render_unmentioned_fields = True
        helper.field_class = 'col-md-12'
        return helper

    def save(self, commit=True):

        ## For link unique to confirmation link
        influence = ''
        event = ''

        if self.cleaned_data.get('influence') and self.cleaned_data.get('event'):
            influence = self.cleaned_data.get('influence')
            event = self.cleaned_data.get('event')

        user = User.objects.create(
            email=self.cleaned_data.get('email'),
            first_name=self.cleaned_data.get('first_name').title(),
            last_name=self.cleaned_data.get('last_name').title(),
        )

        user.set_password(self.cleaned_data.get('password'))
        user.save()
        email = self.cleaned_data.get("email")
        token, created = Token.objects.get_or_create(user=user)
        domain = 'reservas.festivaldelima.com/'
        if influence and event:
            data = {'user': user, 'token': token, 'domain': domain, 'influence': influence, 'event': event}
        else:
            data = {'user': user, 'token': token, 'domain': domain}
        template = loader.get_template('layouts/mailing/welcome.html')
        html = template.render(dict(data))
        subject_user, from_email = 'Bienvenido al Festival de Cine de Lima', 'Festival de Cine de Lima <reservas@reservas.festivaldelima.com>'
        message_user = EmailMessage(subject_user, html, from_email, [email])
        message_user.content_subtype = "html"
        message_user.send(fail_silently=False)
        return user


class UpdateUserForm(forms.ModelForm):
    email = forms.EmailField(required=False, widget=forms.EmailInput(
        attrs={'readonly': 'readonly'}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'phone', 'dni', 'photo', 'musicals')

    def clean_first_name(self):
        if len(self.cleaned_data.get('first_name')) > 0:
            return self.cleaned_data.get('first_name').lower()
        else:
            self.add_error('first_name', 'Ingrese un nombre válido.')

    def clean_last_name(self):
        if len(self.cleaned_data.get('last_name')) > 0:
            return self.cleaned_data.get('last_name').lower()
        else:
            self.add_error('last_name', 'Ingrese un apellido válido.')

    def clean_photo(self):
        photo = self.cleaned_data['photo']

        """
        try:
            w, h = get_image_dimensions(photo)

            # validate dimensions
            max_width = max_height = 200
            if w > max_width or h > max_height:
                raise forms.ValidationError(
                    u'Please use an image that is '
                    '%s x %s pixels or smaller.' % (max_width, max_height))

            # validate content type
            main, sub = photo.content_type.split('/')
            if not (main == 'image' and sub in ['jpeg', 'pjpeg', 'gif', 'png']):
                raise forms.ValidationError(u'Please use a JPEG, '
                                            'GIF or PNG image.')

            # validate file size
            if len(photo) > (20 * 1024):
                raise forms.ValidationError(
                    u'Avatar file size may not exceed 20k.')

        except AttributeError:
            Handles case when we are updating the user profile
            and do not supply a new avatar
            pass
        """

        return photo
