# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations


def capitalize_full_name(apps, schema_editor):
    User = apps.get_model("accounts", "User")
    for usr in User.objects.all():
        usr.first_name = usr.first_name.title()
        usr.last_name = usr.last_name.title()
        usr.save()


class Migration(migrations.Migration):
    dependencies = [
        ('accounts', '0019_auto_20160906_1757'),
    ]

    operations = [
        migrations.RunPython(capitalize_full_name)
    ]
