# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-09-13 11:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0022_musicaltaste'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='musicaltaste',
            options={'verbose_name_plural': 'Gustos Musicales'},
        ),
        migrations.AlterField(
            model_name='musicaltaste',
            name='name',
            field=models.CharField(blank=True, choices=[('t01', 'Rock'), ('t02', 'Indie'), ('t03', 'Electronica'), ('t04', 'Metal'), ('t05', 'Punk'), ('t06', 'Pop'), ('t07', 'Fusion'), ('t08', 'Experimental'), ('t10', 'Reggae'), ('t11', 'Tropical'), ('t12', 'Jazz')], max_length=3),
        ),
    ]
