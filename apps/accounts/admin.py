from django.contrib import admin
from django.utils.translation import ugettext, ugettext_lazy as _
# Register your models here.
from .models import User, Musicals
from django.contrib.auth.admin import UserAdmin
from import_export.admin import ImportExportModelAdmin
from import_export import resources
from import_export.fields import Field


class MyUserResource(resources.ModelResource):
    eventos = Field()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 'date_joined', 'eventos')
        export_order = ('id', 'first_name', 'last_name', 'email', 'date_joined', 'eventos')

    def dehydrate_eventos(self, obj):
        return ','.join(list(obj.tickets.filter(status='c').values_list('event__name', flat=True)))


class MyUserAdmin(UserAdmin, ImportExportModelAdmin):
    list_display = ('date_joined', 'email', 'first_name', 'last_name', 'phone', 'dni', 'gender', 'is_staff', 'user_mode', 'estilos_musicales')
    list_filter = ('user_mode',)
    search_fields = ['email', 'first_name', ]
    ordering = ('-date_joined', 'email')
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'photo', 'gender', 'user_mode',
                                         'event_admin', 'phone')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'confirm', 'is_superuser',
                                       'groups', 'user_permissions')}),

    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )

    resource_class = MyUserResource

    def save_model(self, request, obj, form, change):
        user = obj
        user.save()
        user.send_mail_confirmation(obj)


admin.site.register(User, MyUserAdmin)
admin.site.register(Musicals)
