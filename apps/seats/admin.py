from django.contrib import admin

from apps.seats.models import EventLocation, Section, Seat, ZoneSeat


@admin.register(EventLocation)
class EventLocationAdmin(admin.ModelAdmin):
    raw_id_fields = ('events',)


@admin.register(Section)
class SectionAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'location')
    raw_id_fields = ('location', 'zones',)


@admin.register(Seat)
class SeatAdmin(admin.ModelAdmin):
    list_display = ('id', 'letter', 'number', 'section')
    list_per_page = 50
    list_filter = ('section',)
    search_fields = ('letter',)


@admin.register(ZoneSeat)
class ZoneSeat(admin.ModelAdmin):
    list_filter = ('zone', 'status')
    list_display = ('id', 'zone', 'seat', 'status', 'reservation_date',
                    'sale_date', 'reserved_by', 'bought_by')
    search_fields = ('seat__letter',)
