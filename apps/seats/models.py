from django.db import models

from apps.accounts.models import User
from apps.web.models import Event, Zone


class EventLocation(models.Model):
    name = models.CharField(max_length=100)
    events = models.ManyToManyField(Event)

    def __str__(self):
        return self.name


class Section(models.Model):
    location = models.ForeignKey(EventLocation, on_delete=models.PROTECT,
                                 related_name='sections')
    zones = models.ManyToManyField(Zone)
    name = models.CharField(max_length=50, blank=True)
    template_name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Seat(models.Model):
    section = models.ForeignKey(Section, on_delete=models.PROTECT,
                                related_name='seats')
    letter = models.CharField(max_length=20)
    number = models.IntegerField()
    subsection = models.CharField(max_length=200, blank=True)

    @property
    def identifier(self):
        return '{}{}'.format(self.letter, self.number)

    @property
    def identifier_municipal(self):
        return '{}-{}'.format(self.letter, self.number)

    def __str__(self):
        return '{}: {}{}'.format(self.section.name, self.letter, self.number)


class ZoneSeat(models.Model):
    STATUS_FREE = 'FREE'
    STATUS_RESERVED = 'RESERVERD'
    STATUS_BOUGHT = 'BOUGHT'
    STATUS_CHOICES = (
        (STATUS_FREE, 'free'),
        (STATUS_RESERVED, 'reserved'),
        (STATUS_BOUGHT, 'bought'),
    )

    zone = models.ForeignKey(Zone, on_delete=models.PROTECT,
                             related_name='zone_seats')
    seat = models.ForeignKey(Seat, on_delete=models.PROTECT)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES,
                              default=STATUS_FREE)
    reservation_date = models.DateTimeField(null=True)
    sale_date = models.DateTimeField(null=True)
    reserved_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                                    related_name='reserved_seats')
    bought_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,
                                  related_name='bought_seats')

    @property
    def identifier(self):
        return self.seat.identifier.lower()

    @property
    def identifier_municipal(self):
        return self.seat.identifier_municipal.lower()