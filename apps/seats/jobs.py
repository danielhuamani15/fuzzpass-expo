from logging import getLogger

from apps.seats.utils import free_reserved_seats
from apps.web.models import Event

log = getLogger(__name__)


def free_reserved_seats_job():
    event_amen_teatro = Event.objects.get(id=261)
    event_mirlos = Event.objects.get(id=275)
    event_jaime_cuadra = Event.objects.get(id=398)
    log.info('Freeing seats in event: amen teatro nacional')
    free_reserved_seats(event_amen_teatro, 'free')

    log.info('Freeing seats in event: los mirlos')
    free_reserved_seats(event_mirlos, 'free', '-')

    log.info('Freeing seats in event: los mirlos')
    free_reserved_seats(event_jaime_cuadra, 'free', '-')
