import re
from datetime import datetime, timedelta

from django.db.models import Q
from django.utils import timezone

from apps.payment.models import Ticket, Sale
from apps.seats.models import ZoneSeat


def reserve_seats(zone, selected_seats, user):
    seats = []
    for seat in selected_seats:
        if len(seat.split('-')) == 2:
            letter, number = seat.split('-')[:2]
        else:
            letter, number = re.split('(\d+)', seat)[:2]
        seat_obj = ZoneSeat.objects.get(
            zone=zone,
            seat__letter__iexact=letter,
            seat__number=number,
        )

        seat_obj.status = ZoneSeat.STATUS_RESERVED
        seat_obj.reservation_date = timezone.now().today()
        subsection = ''
        if user:
            seat_obj.reserved_by = user
        seat_obj.save()
        seats.append((seat.upper(), seat_obj.seat.subsection))
    return seats


def update_seats_status(tickets, user):
    from apps.web.models import LogsEvent
    try:
        LogsEvent.objects.create(
            title='Culqui - for tickets',
            data=str(tickets),
            message='recorrido tickets',
            type_log='C',
            nivel='I',
            user=user
        )
        for ticket in tickets:
            if ticket.seat:
                seat = ticket.seat
                zone = ticket.zone
                if len(seat.split('-')) == 2:
                    letter, number = seat.split('-')[:2]
                else:
                    letter, number = re.split('(\d+)', seat)[:2]
                # letter, number = re.split('(\d+)', ticket.seat)[:2]
                seat_obj = ZoneSeat.objects.get(
                    zone=zone,
                    seat__letter__iexact=letter,
                    seat__number=number
                )
                seat_obj.status = ZoneSeat.STATUS_BOUGHT
                seat_obj.bought_by = user
                seat_obj.sale_date = timezone.now().today()
                seat_obj.save()
                data = {
                    'seat_obj': str(seat_obj),
                    'seat': str(seat),
                    'zone': str(zone),
                    'number': str(number),
                    'letter': str(letter)
                }
                LogsEvent.objects.create(
                    title='Culqui - reservar status ticket',
                    data=str(data),
                    message='cambio de status de los tickets',
                    type_log='C',
                    nivel='I',
                    user=user
                )
    except Exception as err:
        LogsEvent.objects.create(
            title='Culqui - reservar status ticket error',
            data=str(err),
            message='error en cambio de status de los tickets',
            type_log='C',
            nivel='E',
            user=user
        )


def free_reserved_seats(event, action, separator=''):
    zones = event.zones.all()
    opened_seats = []

    time_threshold = datetime.now() - timedelta(hours=1)
    reserved_seats = ZoneSeat.objects.filter(
        status=ZoneSeat.STATUS_RESERVED,
        reservation_date__lte=time_threshold,
        zone__in=zones
    ).select_related('seat')
    for reserved_seat in reserved_seats:
        seat = '{}{}{}'.format(
            reserved_seat.seat.letter, separator, reserved_seat.seat.number
        )
        zone = reserved_seat.zone
        if not is_seat_bought(seat, zone):
            if action == 'free':
                free_seat(reserved_seat)
            opened_seats.append([
                zone.events.name, zone, seat,
                reserved_seat.reserved_by, reserved_seat.reservation_date,
                reserved_seat.bought_by, reserved_seat.sale_date
            ])
    return opened_seats


def is_seat_bought(seat, zone):
    tickets = Ticket.objects.filter(
        seat=seat, zone=zone
    )
    if tickets:
        for ticket in tickets:
            if ticket.status == Ticket.Status.c.name or ticket.sales.status == Sale.Status.v.name:
                return True
        return False
    return False


def free_seat(reserved_seat):
    reserved_seat.status = ZoneSeat.STATUS_FREE
    reserved_seat.save()