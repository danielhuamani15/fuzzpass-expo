import csv
from datetime import datetime


from django.core.management.base import BaseCommand

from apps.web.models import Event
from apps.seats.utils import free_reserved_seats


class Command(BaseCommand):
    help = 'Free old reserved seats'

    def add_arguments(self, parser):
        parser.add_argument(
            '--action',
            type=str,
            default='list',
            choices=['list', 'free']
        )

        parser.add_argument(
            '--event_id',
            type=int,
            required=True
        )

        parser.add_argument(
            '--separator',
            type=str,
            default=''
        )

    def handle(self, *args, **options):
        action = options['action']
        event_id = options['event_id']
        separator = options['separator']
        event = Event.objects.get(id=event_id)

        if action == 'list':
            self.stdout.write(self.style.SUCCESS('List seats to free.'))
        else:
            self.stdout.write(self.style.SUCCESS('Free seats.'))

        opened_seats = free_reserved_seats(event, action, separator)
        with open('opened_seats_{}.csv'.format(datetime.now()), 'w') as f:
            csv_writer = csv.writer(f)
            csv_writer.writerow([
                'Event', 'Zone', 'Seat', 'Reserved By', 'Reservation Date',
                'Bought By', 'Bought Date'
            ])
            for row in opened_seats:
                csv_writer.writerow(row)
