# -*- coding: utf-8 -*-

import operator
import logging
import re

from django.conf import settings
from django.utils.cache import patch_vary_headers

from apps.companies.models import Company
from apps.core.utils.site import site_domain_list


logger = logging.getLogger(__name__)
lower = operator.methodcaller('lower')

get_domain_list = site_domain_list

UNSET = object()


class MySubdomainMiddleware(object):
    """
    A middleware class that adds a ``subdomain`` attribute to the current request.
    """
    def get_domains_for_request(self, request):
        """
        Returns the domain that will be used to identify the subdomain part
        for this request.
        """
        return get_domain_list()

    def process_request(self, request):
        """
        Adds a ``subdomain`` attribute to the ``request`` parameter.
        """
        for domain in self.get_domains_for_request(request):
            domain, host = map(lower, (domain, request.get_host()))

            pattern = r'^(?:(?P<subdomain>.*?)\.)?%s(?::.*)?$' \
                      % re.escape(domain)
            matches = re.match(pattern, host)

            if matches:
                request.subdomain = matches.group('subdomain')

                try:
                    request.company = Company.objects.get(
                        subdomain=request.subdomain, whitelabel=True)
                except Company.DoesNotExist:
                    request.company = Company.objects.none()
                return
            else:
                request.subdomain = None
                logger.warning('The host %s does not belong to the domain %s, '
                               'unable to identify the subdomain for this '
                               'request', request.get_host(), domain)


class MySubdomainURLRoutingMiddleware(MySubdomainMiddleware):

    def process_request(self, request):
        """
        Sets the current request's ``urlconf`` attribute to the urlconf
        associated with the subdomain, if it is listed in
        ``settings.SUBDOMAIN_URLCONFS``.
        """
        super(MySubdomainURLRoutingMiddleware, self).process_request(request)

        subdomain = getattr(request, 'subdomain', UNSET)

        if subdomain is not UNSET:
            urlconf = settings.SUBDOMAIN_URLCONFS.get(subdomain)
            if urlconf is not None:
                logger.debug("Using urlconf %s for subdomain: %s",
                             repr(urlconf), repr(subdomain))
                request.urlconf = urlconf

    def process_response(self, request, response):
        """
        Forces the HTTP ``Vary`` header onto requests to avoid having responses
        cached across subdomains.
        """
        if getattr(settings, 'FORCE_VARY_ON_HOST', True):
            patch_vary_headers(response, ('Host',))

        return response
