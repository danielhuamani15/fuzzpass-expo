import locale

from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4
from reportlab.lib.colors import HexColor

from django.conf import settings

BLACK_COLOR = '#000000'
WHITE_COLOR = '#ffffff'
GRAY_COLOR = '#e0e1e2'


class ExportPDF:

    def __init__(self):
        # locale.setlocale(locale.LC_TIME, 'es_PE')
        self.name = None
        self._canvas = None
        self.company_logo = settings.STATIC_ROOT + '/images/23fclnegro.png'
        pdfmetrics.registerFont(TTFont(
            'ProximaNovaRegular',
            settings.STATIC_ROOT + '/fonts/ProximaNovaRegular.ttf'
        ))
        pdfmetrics.registerFont(TTFont(
            'ProximaNovaBold',
            settings.STATIC_ROOT + '/fonts/ProximaNovaBold.ttf'
        ))

    def generate_ticket(self, number, ticket):
        row = int(number / 3) + (number % 3 > 0)
        row = 2 if (row % 2) == 0 else row % 2
        column = 3 if (number % 3) == 0 else number % 3
        h_dimension = 170 * (column - 1)
        v_dimension = 350 * (row - 1)

        self._canvas.setFillColor(HexColor(GRAY_COLOR))
        self._canvas.setStrokeColor(HexColor(GRAY_COLOR))
        self._canvas.rect(
            50 + h_dimension,
            500 - v_dimension,
            150,
            300,
            fill=1
        )

        self._canvas.setFillColor(HexColor(WHITE_COLOR))
        self._canvas.setStrokeColor(HexColor(WHITE_COLOR))
        self._canvas.circle(125 + h_dimension, 800, 20, stroke=1, fill=1)
        self._canvas.drawImage(
            self.company_logo,
            60 + h_dimension,
            770 - v_dimension,
            40,
            20,
            mask='auto'
        )

        self._canvas.setFillColor(HexColor(BLACK_COLOR))
        self._canvas.setFont('ProximaNovaRegular', 10)
        self._canvas.drawRightString(
            190 + h_dimension,
            780 - v_dimension,
            'Invitación'
        )

        if ticket.seat:
            self._canvas.setFont('ProximaNovaRegular', 6)
            self._canvas.drawRightString(
                190 + h_dimension,
                770 - v_dimension,
                'Asiento: %s' % ticket.seat
            )
        # if ticket.seat_section:
        #     self._canvas.setFont('ProximaNovaRegular', 6)
        #     self._canvas.drawRightString(
        #         190 + h_dimension,
        #         775 - v_dimension,
        #         'Zona: %s' % ticket.seat_section
        #     )

        zone_name = ticket.zone.name
        if len(zone_name) > 30:
            zone_name = 'Zona:' + ticket.zone.name[:30] + '...'

        self._canvas.setFillColor(HexColor(BLACK_COLOR))
        self._canvas.setFont('ProximaNovaRegular', 6)
        self._canvas.drawRightString(
            190 + h_dimension,
            760 - v_dimension,
            zone_name
        )

        self._canvas.setFillColor(HexColor(BLACK_COLOR))
        self._canvas.setFont('ProximaNovaRegular', 6)
        self._canvas.drawString(
            60 + h_dimension,
            740 - v_dimension,
            ticket.event.start_date.strftime('%a, %d DE %b / %H:%M').upper()
        )

        self._canvas.setFillColor(HexColor(BLACK_COLOR))
        self._canvas.setFont('ProximaNovaBold', 9)

        event_name = ticket.event.name
        
        self._canvas.setFont('ProximaNovaRegular', 6.5)
        self._canvas.drawCentredString(
            123 + h_dimension,
            730 - v_dimension,
            event_name
        )

        self._canvas.drawImage(
            ticket.qr.path,
            70 + h_dimension,
            600 - v_dimension,
            110,
            110,
            mask='auto'
        )

        self._canvas.setFont('ProximaNovaRegular', 9)
        self._canvas.drawCentredString(
            125 + h_dimension,
            580 - v_dimension,
            ticket.code
        )
        self._canvas.setFont('ProximaNovaRegular', 6)
        self._canvas.drawCentredString(
            125 + h_dimension,
            560 - v_dimension,
            'Valida tu ticket solo en la puerta del evento'
        )
        self._canvas.setFont('ProximaNovaRegular', 6)
        # self._canvas.drawCentredString(
        #     125 + h_dimension,
        #     540 - v_dimension,
        #     'Un evento de %s' % ticket.event.organizer.name
        # )

        self._canvas.drawImage(
            ticket.event.organizer.logo.path,
            115 + h_dimension,
            510 - v_dimension,
            20,
            20,
            mask='auto'
        )
        if (number % 6) == 0:
            canvas.showPage()

    def create(self, sale):
        self.name = 'tickets/pdf/tickets-%s.pdf' % sale.id
        self._canvas = canvas.Canvas(
            settings.MEDIA_ROOT + '/' + self.name,
            pagesize=A4
        )
        number = 1
        for ticket in sale.tickets_sales.all():
            self.generate_ticket(number, ticket)
            number += 1

        self._canvas.save()
        sale.pdf.name = self.name
        sale.save()
        return sale.pdf.path
