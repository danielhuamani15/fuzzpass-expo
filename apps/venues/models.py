from django.contrib.gis.db import models
from django.utils.translation import ugettext_lazy as _


class Venue(models.Model):
    name = models.CharField(
        max_length=200
    )
    address = models.CharField(
        max_length=200
    )
    city = models.ForeignKey(
        'web.city'
    )
    state = models.CharField(
        max_length=50,
        blank=True
    )
    telephone1 = models.CharField(
        max_length=11,
        blank=True
    )
    telephone2 = models.CharField(
        max_length=11,
        blank=True
    )
    location = models.PointField(
        'longitude/latitude',
        geography=True, null=True, blank=True
    )
    map_image = models.ImageField(
        upload_to='event/maps',
        help_text="Ingrese la imagen del mapa del evento",
        verbose_name="Imagen del mapa del evento",
        blank=True,
        null=True
    )
    company = models.ForeignKey(
        'companies.Company',
        null=True,
        blank=True
    )
    active = models.BooleanField(
        default=True
    )

    def __str__(self):
        return self.name

    @property
    def full_address(self):
        if self.state:
            return '%s - %s, %s, %s' % (self.name, self.address, self.state, self.city)
        return '%s - %s, %s' % (self.name, self.address, self.city)

    class Meta:
        db_table = 'venue'
        verbose_name = _('venue')
        verbose_name_plural = _('venues')

