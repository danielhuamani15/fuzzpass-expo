# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2017-07-06 20:00
from __future__ import unicode_literals

import django.contrib.gis.db.models.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('venues', '0003_auto_20170706_1842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='venue',
            name='location',
            field=django.contrib.gis.db.models.fields.PointField(blank=True, geography=True, null=True, srid=4326, verbose_name='longitude/latitude'),
        ),
        migrations.AlterField(
            model_name='venue',
            name='name',
            field=models.CharField(max_length=200),
        ),
    ]
