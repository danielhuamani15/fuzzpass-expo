from django.contrib import admin

from apps.venues.models import Venue
from apps.web.forms import LocationForm


class VenueInLine(admin.StackedInline):
    model = Venue
    extra = 0


@admin.register(Venue)
class VenueAdmin(admin.ModelAdmin):
    form = LocationForm
    list_display = ('company', 'name', 'address', 'city', 'state', 'active')
    search_fields = ('name', )
    raw_id_fields = ('company', )

    fieldsets = (
        (None, {
            'fields': (
                'name',
                'address',
                'city',
                'state',
                'telephone1',
                'telephone2',
                'location',
                'map_image',
                'company',
                'active'
            )
        }),
    )
