#!/bin/bash
set -e

DB_NAME="fuzzpass"
POSTGRES_USER="${POSTGRES_USER:-postgres}"

echo "CREATING ${DB_NAME} DB for admin user ${POSTGRES_USER}"
echo "------"

psql -v ON_ERROR_STOP=0 --username "$POSTGRES_USER" <<-EOSQL
    CREATE ROLE fuzzpass WITH SUPERUSER LOGIN PASSWORD 'fuzzpass';
EOSQL

psql -v ON_ERROR_STOP=0 --username "$POSTGRES_USER" --dbname template1 <<-EOSQL
    CREATE EXTENSION postgis;
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE DATABASE $DB_NAME;
    GRANT CONNECT ON DATABASE $DB_NAME TO fuzzpass;
    ALTER DATABASE $DB_NAME OWNER TO fuzzpass;
    GRANT ALL PRIVILEGES ON DATABASE $DB_NAME TO fuzzpass;
EOSQL
