from __future__ import print_function
from fabric.api import *

env.colorize_errors = True
settings = None


def devops_settings(mode='staging'):
    env.use_ssh_config = True
    env.forward_agent = True
    if mode == 'staging':
        env.hosts = ['fuzzpass-staging']
    else:
        env.hosts = ['fuzzpass']
    env.user = 'devops'
    env.user_group = 'devops'
    env.user_name = 'devops'


def deploy():
    project_name = 'fuzzpass'
    virtualenv = 'fuzzpass'
    git_url = 'git@gitlab.com:fuzzpass/fuzzpass.git'

    with cd('projects/{}'.format(project_name)):
        run('git checkout master')
        run('git pull {}'.format(git_url))
        with prefix('workon {}'.format(virtualenv)):
            run('pip install -r requirements/production.txt')
            run('python manage.py collectstatic --settings=fuzzpass.settings.production')
            run('python manage.py migrate --settings=fuzzpass.settings.production')
            run('npm i')
            run('bower i')
            run('gulp build')
        sudo('supervisorctl restart all')
        sudo('service nginx restart')


def deploy_staging():
    devops_settings(mode='staging')
    deploy()


def deploy_production():
    devops_settings(mode='production')
    deploy()
